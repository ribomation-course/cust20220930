package ribomation.web.controllers;

//...

public class PersonController {
    @Autowired private PersonDAO dao;

    @GetMapping
    public String index() {
        return "redirect:/list";
    }

    @GetMapping("list")
    public String list(Model data) {
        var lst = //...
        data.addAttribute("persons", /*...*/);
        return "list";
    }

    @GetMapping("show/{id}")
    public String show(@PathVariable int id, Model data) {
        var obj = //...
        if (obj.isPresent()) {
            data.addAttribute("person", /*...*/);
            return "show";
        }
        return "redirect:/list";
    }

    @GetMapping("edit/{id}")
    public String edit(@PathVariable int id, Model data) {
        var obj = //...
        if (obj.isEmpty()) {
            throw new RuntimeException("not found");
        }
        data.addAttribute("person", /*...*/);
        return "edit";
    }

    @PostMapping("save/{id}")
    public String save(@PathVariable int id, Person delta) {
        if (id == -1) {
            var p = dao.create();
            dao.insert(p);
            dao.update(p.getId(), delta);
            return "redirect:/show/" + p.getId();
        } else {
            if (dao.existsById(id)) {
                dao.update(id, delta);
            } else {
                throw new RuntimeException("not found");
            }
            return "redirect:/show/" + id;
        }
    }

    @GetMapping("remove/{id}")
    public String remove(@PathVariable int id, Model data) {
        var obj = //...
        if (obj.isEmpty()) {
            throw new RuntimeException("not found");
        }
        //...delete...
        return "redirect:/";
    }
}
