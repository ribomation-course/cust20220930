package ribomation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ribomation.domain.PersonCsvMapper;

import static org.junit.jupiter.api.Assertions.*;

class PersonCsvMapperTest {
    PersonCsvMapper target;

    @BeforeEach
    void setUp() {
        target = new PersonCsvMapper(";");
    }

    @Test
    void fromCSV() {
        var csv = "Riley Fettis;64;Female;70664";
        var p = target.fromCSV(csv);
        assertEquals("Riley Fettis", p.getName());
        assertTrue(p.isFemale());
        assertEquals(70664, p.getPostCode());
    }

}
