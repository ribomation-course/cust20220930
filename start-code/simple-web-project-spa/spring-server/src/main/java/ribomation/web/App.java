package ribomation.web;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import ribomation.web.domain.PersonDAO;

@SpringBootApplication
public class App implements WebMvcConfigurer {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @Bean
    CommandLineRunner setup(PersonDAO dao) {
        return args -> {
            dao.populate(8);
            System.out.printf("generated %d persons%n", dao.count());
        };
    }
}
