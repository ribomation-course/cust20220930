package ribomation.web.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Person {
    private Integer id;
    private String name;
    private Integer age;
    private String avatar;
    private String company;
    private String email;

    public Person withAvatarSize(int sz) {
        var ix = avatar.lastIndexOf("=");
        var url = avatar.substring(0, ix);
        var img = String.format("%s=%dx%d", url, sz, sz);
        return new Person(getId(), getName(), getAge(), img, getCompany(), getEmail());
    }
}
