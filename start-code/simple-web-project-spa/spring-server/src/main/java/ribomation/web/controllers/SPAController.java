package ribomation.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SPAController {
    @GetMapping({
            "/list",
            "/show/*",
            "/edit/*"
    })
    public String forwardToIndex() {
        return "forward:/index.html";
    }
}


