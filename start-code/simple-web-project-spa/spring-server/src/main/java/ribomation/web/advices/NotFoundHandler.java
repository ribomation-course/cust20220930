package ribomation.web.advices;

import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;

//@ControllerAdvice
public class NotFoundHandler {

//    @ExceptionHandler(NoHandlerFoundException.class)
//    public ResponseEntity<String> renderSpaIndexPage(HttpServletRequest req) {
//        final String INDEX = "classpath:/vue/index.html";
//        System.out.printf("*** [404 handler] REQ: %s %s%n", req.getMethod(), req.getRequestURI());
//
//        try {
//            var loader = new DefaultResourceLoader();
//            var res = loader.getResource(INDEX);
//            var body = StreamUtils.copyToString(res.getInputStream(), StandardCharsets.UTF_8);
//            return ResponseEntity
//                    .ok()
//                    .contentType(MediaType.TEXT_HTML)
//                    .body(body);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return ResponseEntity
//                    .internalServerError()
//                    .body("Cannot load index resource");
//        }
//    }

}
