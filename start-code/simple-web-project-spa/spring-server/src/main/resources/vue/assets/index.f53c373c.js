import { r as resolveComponent, c as createElementBlock, a as createVNode, F as Fragment, b as createBaseVNode, o as openBlock, p as pushScopeId, d as popScopeId, u as unref, C as C__ribomationCourses2020Autumn_jvm_springFramework2021_explorations_sampleWebSpa_vueClient_node_modules_axios, e as ref, f as onMounted, w as withCtx, g as renderList, t as toDisplayString, h as withModifiers, i as createTextVNode, j as useRouter, k as withDirectives, v as vModelText, l as createRouter, m as createWebHistory, n as createApp } from "./vendor.d3da78f6.js";
const _sfc_main$4 = {};
const _hoisted_1$4 = /* @__PURE__ */ createBaseVNode("nav", null, [
  /* @__PURE__ */ createBaseVNode("span", { class: "brand" }, "Silly Persons - Spring App"),
  /* @__PURE__ */ createBaseVNode("a", {
    href: "/list",
    class: "float-right"
  }, "[List of Persons]")
], -1);
function _sfc_render(_ctx, _cache) {
  const _component_router_view = resolveComponent("router-view");
  return openBlock(), createElementBlock(Fragment, null, [
    _hoisted_1$4,
    createVNode(_component_router_view)
  ], 64);
}
_sfc_main$4.render = _sfc_render;
var notFoundUrl = "/assets/not-found.8fd07c28.png";
var notFound_vue_vue_type_style_index_0_scoped_true_lang = "\nimg[data-v-03da6d2a] {\r\n    width: 20em;\n}\r\n";
pushScopeId("data-v-03da6d2a");
const _hoisted_1$3 = /* @__PURE__ */ createBaseVNode("h1", null, "Page Not Found", -1);
const _hoisted_2$3 = ["src"];
popScopeId();
const _sfc_main$3 = {
  setup(__props) {
    const image = notFoundUrl;
    return (_ctx, _cache) => {
      return openBlock(), createElementBlock("main", null, [
        _hoisted_1$3,
        createBaseVNode("div", null, [
          createBaseVNode("img", {
            src: unref(image),
            alt: "not found",
            class: "img-fluid"
          }, null, 8, _hoisted_2$3)
        ])
      ]);
    };
  }
};
_sfc_main$3.__scopeId = "data-v-03da6d2a";
const url = "http://localhost:8080/api/persons";
async function findAll() {
  const response = await C__ribomationCourses2020Autumn_jvm_springFramework2021_explorations_sampleWebSpa_vueClient_node_modules_axios.get(url);
  if (response.status === 200) {
    return response.data;
  } else {
    console.error("failed: %d %s", response.status, response.statusText);
    return [];
  }
}
async function findById(id) {
  const response = await C__ribomationCourses2020Autumn_jvm_springFramework2021_explorations_sampleWebSpa_vueClient_node_modules_axios.get(`${url}/${id}`);
  if (response.status === 200) {
    return response.data;
  } else {
    console.error("failed: %d %s", response.status, response.statusText);
    return {};
  }
}
async function insert(product) {
  const response = await C__ribomationCourses2020Autumn_jvm_springFramework2021_explorations_sampleWebSpa_vueClient_node_modules_axios.post(url, product);
  if (response.status === 201) {
    return response.data;
  } else {
    console.error("failed: %d %s", response.status, response.statusText);
    return {};
  }
}
async function update(id, product) {
  const response = await C__ribomationCourses2020Autumn_jvm_springFramework2021_explorations_sampleWebSpa_vueClient_node_modules_axios.put(`${url}/${id}`, product);
  if (response.status === 200) {
    return response.data;
  } else {
    console.error("failed: %d %s", response.status, response.statusText);
    return void 0;
  }
}
async function remove(id) {
  const response = await C__ribomationCourses2020Autumn_jvm_springFramework2021_explorations_sampleWebSpa_vueClient_node_modules_axios.delete(`${url}/${id}`);
  if (response.status === 204) {
    return true;
  } else {
    console.error("failed: %d %s", response.status, response.statusText);
    return void 0;
  }
}
const _hoisted_1$2 = /* @__PURE__ */ createBaseVNode("span", null, "List of Persons", -1);
const _hoisted_2$2 = /* @__PURE__ */ createTextVNode(" Create ");
const _hoisted_3$2 = /* @__PURE__ */ createBaseVNode("thead", null, [
  /* @__PURE__ */ createBaseVNode("tr", null, [
    /* @__PURE__ */ createBaseVNode("th", { class: "center" }, "#"),
    /* @__PURE__ */ createBaseVNode("th", null, "Avatar"),
    /* @__PURE__ */ createBaseVNode("th", null, "Name"),
    /* @__PURE__ */ createBaseVNode("th", { class: "center" }, "Age"),
    /* @__PURE__ */ createBaseVNode("th", null, "Company"),
    /* @__PURE__ */ createBaseVNode("th", null, "Email"),
    /* @__PURE__ */ createBaseVNode("th", { class: "right" }, [
      /* @__PURE__ */ createBaseVNode("em", null, "Actions")
    ])
  ])
], -1);
const _hoisted_4$2 = { class: "center" };
const _hoisted_5$2 = { style: { "width": "4em" } };
const _hoisted_6$2 = ["src"];
const _hoisted_7$2 = { class: "name" };
const _hoisted_8$1 = { class: "center" };
const _hoisted_9$1 = { class: "right" };
const _hoisted_10$1 = /* @__PURE__ */ createTextVNode(" Show ");
const _hoisted_11$1 = ["onClick"];
const _sfc_main$2 = {
  setup(__props) {
    const persons = ref([]);
    const loadAll = async () => {
      persons.value = await findAll();
    };
    const removeItem = async (id) => {
      await remove(id);
      await loadAll();
    };
    onMounted(loadAll);
    return (_ctx, _cache) => {
      const _component_router_link = resolveComponent("router-link");
      return openBlock(), createElementBlock(Fragment, null, [
        createBaseVNode("h1", null, [
          _hoisted_1$2,
          createVNode(_component_router_link, {
            to: "/edit/-1",
            class: "btn btn-success float-right"
          }, {
            default: withCtx(() => [
              _hoisted_2$2
            ]),
            _: 1
          })
        ]),
        createBaseVNode("table", null, [
          _hoisted_3$2,
          createBaseVNode("tbody", null, [
            (openBlock(true), createElementBlock(Fragment, null, renderList(persons.value, (p) => {
              return openBlock(), createElementBlock("tr", {
                key: p.id
              }, [
                createBaseVNode("td", _hoisted_4$2, toDisplayString(p.id), 1),
                createBaseVNode("td", _hoisted_5$2, [
                  createBaseVNode("img", {
                    src: p.avatar,
                    class: "avatar"
                  }, null, 8, _hoisted_6$2)
                ]),
                createBaseVNode("td", _hoisted_7$2, toDisplayString(p.name), 1),
                createBaseVNode("td", _hoisted_8$1, toDisplayString(p.age), 1),
                createBaseVNode("td", null, toDisplayString(p.company), 1),
                createBaseVNode("td", null, toDisplayString(p.email), 1),
                createBaseVNode("td", _hoisted_9$1, [
                  createVNode(_component_router_link, {
                    to: "/show/" + p.id,
                    class: "btn"
                  }, {
                    default: withCtx(() => [
                      _hoisted_10$1
                    ]),
                    _: 2
                  }, 1032, ["to"]),
                  createBaseVNode("a", {
                    onClick: withModifiers(($event) => removeItem(p.id), ["prevent"]),
                    class: "btn btn-danger"
                  }, " Remove ", 8, _hoisted_11$1)
                ])
              ]);
            }), 128))
          ])
        ])
      ], 64);
    };
  }
};
const _hoisted_1$1 = { class: "container" };
const _hoisted_2$1 = {
  class: "card",
  style: { "width": "25em" }
};
const _hoisted_3$1 = ["src"];
const _hoisted_4$1 = { class: "center" };
const _hoisted_5$1 = { class: "buttons" };
const _hoisted_6$1 = ["onClick"];
const _hoisted_7$1 = /* @__PURE__ */ createTextVNode(" Edit ");
const _sfc_main$1 = {
  props: {
    id: String
  },
  setup(__props) {
    const props = __props;
    const router2 = useRouter();
    const person = ref({});
    const cancel = () => {
      router2.back();
    };
    const loadItem = async () => {
      person.value = await findById(props.id);
    };
    const removeItem = async (id) => {
      await remove(id);
      cancel();
    };
    onMounted(loadItem);
    return (_ctx, _cache) => {
      const _component_router_link = resolveComponent("router-link");
      return openBlock(), createElementBlock("div", _hoisted_1$1, [
        createBaseVNode("h1", null, "Person ID: " + toDisplayString(person.value.id), 1),
        createBaseVNode("div", _hoisted_2$1, [
          createBaseVNode("img", {
            src: person.value.avatar
          }, null, 8, _hoisted_3$1),
          createBaseVNode("h1", null, toDisplayString(person.value.name), 1),
          createBaseVNode("h2", null, toDisplayString(person.value.email), 1),
          createBaseVNode("h3", null, toDisplayString(person.value.company), 1),
          createBaseVNode("p", _hoisted_4$1, "Age " + toDisplayString(person.value.age), 1),
          createBaseVNode("div", _hoisted_5$1, [
            createBaseVNode("a", {
              onClick: withModifiers(cancel, ["prevent"]),
              class: "btn btn-secondary"
            }, " Cancel ", 8, _hoisted_6$1),
            createVNode(_component_router_link, {
              to: "/edit/" + person.value.id,
              class: "btn"
            }, {
              default: withCtx(() => [
                _hoisted_7$1
              ]),
              _: 1
            }, 8, ["to"]),
            createBaseVNode("a", {
              onClick: _cache[0] || (_cache[0] = withModifiers(($event) => removeItem(person.value.id), ["prevent"])),
              class: "btn btn-danger"
            }, " Remove ")
          ])
        ])
      ]);
    };
  }
};
const _hoisted_1 = { class: "container" };
const _hoisted_2 = {
  class: "card",
  style: { "width": "30em" }
};
const _hoisted_3 = { class: "center" };
const _hoisted_4 = ["src"];
const _hoisted_5 = { style: { "padding": "1em" } };
const _hoisted_6 = { for: "name" };
const _hoisted_7 = /* @__PURE__ */ createTextVNode("full name ");
const _hoisted_8 = { for: "email" };
const _hoisted_9 = /* @__PURE__ */ createTextVNode("email address ");
const _hoisted_10 = { for: "company" };
const _hoisted_11 = /* @__PURE__ */ createTextVNode("company name ");
const _hoisted_12 = { for: "age" };
const _hoisted_13 = /* @__PURE__ */ createTextVNode("age ");
const _hoisted_14 = { class: "buttons" };
const _hoisted_15 = ["onClick"];
const _hoisted_16 = ["onClick"];
const _sfc_main = {
  props: {
    id: Number
  },
  setup(__props) {
    const props = __props;
    const router2 = useRouter();
    const person = ref({});
    const cancel = () => {
      router2.back();
    };
    const loadItem = async () => {
      person.value = await findById(props.id);
    };
    const saveItem = async () => {
      if (Number(props.id) === -1) {
        await insert(person.value);
      } else {
        await update(props.id, person.value);
      }
      cancel();
    };
    onMounted(loadItem);
    return (_ctx, _cache) => {
      return openBlock(), createElementBlock("form", null, [
        createBaseVNode("div", _hoisted_1, [
          createBaseVNode("h1", null, "Person ID: " + toDisplayString(person.value.id), 1),
          createBaseVNode("div", _hoisted_2, [
            createBaseVNode("div", _hoisted_3, [
              createBaseVNode("img", {
                src: person.value.avatar,
                class: "avatar",
                style: { "width": "15em" }
              }, null, 8, _hoisted_4)
            ]),
            createBaseVNode("div", _hoisted_5, [
              createBaseVNode("label", _hoisted_6, [
                _hoisted_7,
                withDirectives(createBaseVNode("input", {
                  type: "text",
                  id: "name",
                  name: "name",
                  "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => person.value.name = $event)
                }, null, 512), [
                  [vModelText, person.value.name]
                ])
              ]),
              createBaseVNode("label", _hoisted_8, [
                _hoisted_9,
                withDirectives(createBaseVNode("input", {
                  type: "email",
                  id: "email",
                  name: "email",
                  "onUpdate:modelValue": _cache[1] || (_cache[1] = ($event) => person.value.email = $event)
                }, null, 512), [
                  [vModelText, person.value.email]
                ])
              ]),
              createBaseVNode("label", _hoisted_10, [
                _hoisted_11,
                withDirectives(createBaseVNode("input", {
                  type: "text",
                  id: "company",
                  name: "company",
                  "onUpdate:modelValue": _cache[2] || (_cache[2] = ($event) => person.value.company = $event)
                }, null, 512), [
                  [vModelText, person.value.company]
                ])
              ]),
              createBaseVNode("label", _hoisted_12, [
                _hoisted_13,
                withDirectives(createBaseVNode("input", {
                  type: "number",
                  id: "age",
                  name: "age",
                  "onUpdate:modelValue": _cache[3] || (_cache[3] = ($event) => person.value.age = $event),
                  min: "20",
                  max: "85"
                }, null, 512), [
                  [vModelText, person.value.age]
                ])
              ])
            ]),
            createBaseVNode("div", _hoisted_14, [
              createBaseVNode("a", {
                onClick: withModifiers(cancel, ["prevent"]),
                class: "btn btn-secondary"
              }, " Cancel ", 8, _hoisted_15),
              createBaseVNode("a", {
                onClick: withModifiers(saveItem, ["prevent"]),
                class: "btn btn-success"
              }, " Save ", 8, _hoisted_16)
            ])
          ])
        ])
      ]);
    };
  }
};
const routes = [
  { path: "/list", name: "list", component: _sfc_main$2 },
  { path: "/show/:id", name: "show", props: true, component: _sfc_main$1 },
  { path: "/edit/:id", name: "edit", props: true, component: _sfc_main },
  { path: "/", redirect: { name: "list" } },
  { path: "/:pathMatch(.*)*", component: _sfc_main$3 }
];
const router = createRouter({
  history: createWebHistory(),
  routes
});
var style = "body {\r\n    font-family: sans-serif;\r\n    box-sizing: border-box;\r\n}\r\n\r\ntable {\r\n    width: 100%;\r\n}\r\n\r\nthead tr {\r\n    background-color: #333;\r\n    color: #fff;\r\n}\r\n\r\nthead th {\r\n    padding: .25em;\r\n}\r\n\r\ntbody tr:hover {\r\n    background-color: #eee;\r\n}\r\n\r\nimg {\r\n    width: 100%;\r\n}\r\n\r\nimg.avatar {\r\n    border-radius: 50%;\r\n}\r\n\r\n.name {\r\n    font-size: 1.25rem;\r\n    font-weight: 600;\r\n}\r\n\r\n.btn {\r\n    display: inline-block;\r\n    background-color: hsl(210, 100%, 45%);\r\n    color: white;\r\n    font-size: 1rem;\r\n    font-weight: 400;\r\n    padding: .35rem 1rem;\r\n    border-radius: .2rem;\r\n}\r\n\r\n.btn:hover {\r\n    box-shadow: 2px 2px 4px #777;\r\n    border: 1px solid #222;\r\n}\r\n\r\na.btn {\r\n    text-decoration: none;\r\n}\r\n\r\n.btn.btn-danger {\r\n    background-color: hsl(355, 100%, 45%);\r\n}\r\n\r\n.btn.btn-success {\r\n    background-color: hsl(105, 100%, 30%);\r\n}\r\n\r\n.btn.btn-secondary {\r\n    background-color: hsl(0, 0%, 45%);\r\n}\r\n\r\n.btn + .btn {\r\n    margin-left: .5em;\r\n}\r\n\r\n.left {\r\n    text-align: left;\r\n}\r\n\r\n.center {\r\n    text-align: center;\r\n}\r\n\r\n.right {\r\n    text-align: right;\r\n}\r\n\r\n.container {\r\n    padding: 1em;\r\n}\r\n\r\n.card {\r\n    border: 1px solid #333333;\r\n    box-shadow: 2px 2px 4px #777777;\r\n    margin: 1em auto;\r\n}\r\n\r\n.buttons {\r\n    text-align: center;\r\n    background-color: #eee;\r\n    padding: .5em 1em;\r\n}\r\n\r\nh1, h2, h3 {\r\n    text-align: center;\r\n    margin: .1em 0;\r\n    overflow-x: hidden;\r\n}\r\n\r\nlabel {\r\n    display: block;\r\n    font-weight: 600;\r\n    font-variant: small-caps;\r\n    text-transform: capitalize;\r\n}\r\n\r\nlabel + label {\r\n    margin-top: 1em;\r\n}\r\n\r\nlabel + .buttons {\r\n    margin-top: 2em;\r\n}\r\n\r\nlabel input {\r\n    display: block;\r\n    width: 95%;\r\n    padding: .25em .5em;\r\n    border-radius: .2em;\r\n}\r\n\r\nnav {\r\n    background-color: hsl(210, 100%, 30%);\r\n    color: #fff;\r\n    display: block;\r\n    padding: 1em;\r\n}\r\n\r\nnav .brand {\r\n    font-weight: 600;\r\n}\r\n\r\n.float-right {\r\n    float: right;\r\n}\r\n\r\nnav a {\r\n    text-decoration: none;\r\n    color: #fff;\r\n}\r\n\r\nnav a:hover {\r\n    color: darkorange;\r\n}\r\n\r\n";
const app = createApp(_sfc_main$4);
app.use(router);
app.mount("#app");
