package ribomation.nano_spring.impl;

public final class BeanWrapper {
    final String name;
    final Object object;

    public BeanWrapper(String name, Object object) {
        this.name = name;
        this.object = object;
    }
}
