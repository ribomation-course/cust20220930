package ribomation.nano_spring.api.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

public class GenericRowMapper<DomainClass> extends GeneratorSupport {
    private final Class<DomainClass> domainClass;

    public GenericRowMapper(DomainClass sampleObj) {
        domainClass = (Class<DomainClass>) sampleObj.getClass();
    }

    public DomainClass mapRow(ResultSet rs) {
        try {
            var domainObj   = domainClass.getConstructor().newInstance();
            var metaData    = rs.getMetaData();
            var columnCount = metaData.getColumnCount();
            for (var k = 1; k <= columnCount; ++k) {
                var name   = metaData.getColumnName(k);
                var clazz  = metaData.getColumnClassName(k);
                var setter = domainClass.getMethod(toSetterName(name), toClassJava(clazz));
                setter.invoke(domainObj, rs.getObject(name, toClassJdbc(clazz)));
            }
            return domainClass.cast(domainObj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public PreparedStatement prepare(DomainClass domainObject, List<String> columns, PreparedStatement ps) {
        try {
            var idx = 1;
            for (String col : columns) {
                var getterName = toGetterName(col);
                var getter     = domainClass.getMethod(getterName);
                var value      = getter.invoke(domainObject);
                ps.setObject(idx++, value);
            }
            return ps;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    //N.B., very limited/incomplete mapping
    private Class toClassJdbc(String className) throws ClassNotFoundException {
        if ("java.sql.Timestamp".equals(className)) return java.util.Date.class;
        if ("java.lang.Integer".equals(className)) return Integer.class;
        if ("java.lang.Long".equals(className)) return Long.class;
        if ("java.lang.String".equals(className)) return String.class;
        return Class.forName(className);
    }

    //N.B., very limited/incomplete mapping
    private Class toClassJava(String className) throws ClassNotFoundException {
        if ("java.sql.Timestamp".equals(className)) return java.util.Date.class;
        if ("java.lang.Integer".equals(className)) return int.class;
        if ("java.lang.Long".equals(className)) return long.class;
        if ("java.lang.String".equals(className)) return String.class;
        return Class.forName(className);
    }

    public String toSetterName(String s) {
        return toAccessorName("set", s);
    }

    public String toGetterName(String s) {
        return toAccessorName("get", s);
    }

    private String toAccessorName(String prefix, String name) {
        if (name == null || name.length() < 2) {
            throw new IllegalArgumentException("cannot be a accessible property: " + name);
        }
        return prefix + name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
    }
}
