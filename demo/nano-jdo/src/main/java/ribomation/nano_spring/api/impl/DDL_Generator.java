package ribomation.nano_spring.api.impl;

import ribomation.nano_spring.api.Column;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.String.format;

public class DDL_Generator extends GeneratorSupport {
    public String generateCreateTable(Class domainClass) {
        var tableName = getTableName(domainClass);
        var columns = getColumnsDescriptors(domainClass);
        var max = columns.stream()
                .mapToInt(c -> c.columnName().length())
                .max()
                .orElseThrow();

        return format("CREATE TABLE IF NOT EXISTS %s (%n%s%n);",
                tableName,
                columns.stream()
                        .map(c -> format("  %-" + max + "s  %s",
                                c.columnName(), c.columnType()))
                        .collect(Collectors.joining("," + System.lineSeparator()))
        );
    }

    private List<ColumnDescriptor> getColumnsDescriptors(Class domainClass) {
        return Stream.of(domainClass.getDeclaredFields())
                .filter(field -> field.getAnnotation(Column.class) != null)
                .map(ColumnDescriptor::new)
                .collect(Collectors.toList());
    }
}

