package ribomation.nano_spring.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class PropertiesLoaderTest {
    PropertiesLoader loader;
    BeanRepositoryImpl repo;

    @BeforeEach
    void setUp() {
        repo = new BeanRepositoryImpl();
        loader = new PropertiesLoader(repo);
    }

    @Test
    @DisplayName("Loading a single bean, should work")
    void load1() {
        var def = "pelle=ribomation.domain.Cat;Pelle Svanslos\n";
        var buf = new ByteArrayInputStream(def.getBytes(StandardCharsets.UTF_8));
        loader.load(buf);

        assertEquals(1, repo.getDefinitions().size());

        var entry = repo.getDefinitions().entrySet().iterator().next();
        assertEquals("pelle", entry.getKey());
        assertEquals("ribomation.domain.Cat", entry.getValue().beanClass);
        assertEquals("Pelle Svanslos", entry.getValue().constructorArgs.get(0));
    }

    @Test
    @DisplayName("Loading three beans, should work")
    void load2() {
        var defs = String.join("\n",
                "cat-1=ribomation.domain.Cat;Haskel",
                "cat-2=ribomation.domain.Cat;C++",
                "cat-3=ribomation.domain.Cat;Perl"
        );

        var buf = new ByteArrayInputStream(defs.getBytes(StandardCharsets.UTF_8));
        loader.load(buf);

        assertEquals(3, repo.getDefinitions().size());
        var expectedNames = new String[]{"cat-1", "cat-2", "cat-3"};
        var actualNames = repo.getDefinitions().keySet().toArray();
        assertArrayEquals(expectedNames, actualNames);
    }

}
