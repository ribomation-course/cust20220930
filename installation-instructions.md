# Installation Instructions

In order to participate and perform the programming exercises of the course,
you need to have the following installed.

## Zoom Client
* https://us02web.zoom.us/download
* [Video/Audio Configuration](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/zoom-configuration.md)

## GIT Client
* https://git-scm.com/downloads

## IDE
A decent IDE, such as any of

* JetBrains IntelliJ IDEA
    - https://www.jetbrains.com/idea/download
* MicroSoft Visual Code
    - https://code.visualstudio.com/download
* Eclipse
    - https://www.eclipse.org/ide/
* Something else, you already are familiar with

## Java JDK
The decent version of Java JDK installed, such as v18, appropriate for Spring
* [Java JDK Download](https://jdk.java.net/18/)

## Access to Spring Initializr Web
Ability to configure and download a project ZIP from https://start.spring.io/
Check also access to regular Maven repositories and the Spring 

## Build Tool
Need to have one of the build tools Maven or Gradle installed. However,
when generating a project via Spring Initializr, there is a wrapper shell
script to use.

* Gradle: `gradlew` / `gradle.bat`
* Maven: `mvnw` / `mvnw.cmd`

# Instruktioner från SKV
Följande PDF dokument kommer från Safi Ahlberg och innehåller 
instruktioner för att anpassa er miljö.

* [Lite fix och anpassningar för SKV](./pdf/lite-fix-och-anpassningar.pdf)


