# Hemuppgift 1
Färdigställ under vecka 40, före nästa kursdag.
Arbeta gärna tillsammans med en kollega.

----
### [Lösningsförslag hemuppgift 1](./solution/)

----

## Innan du börjar
Gör klart de övningsuppgifter du inte hann med under kursdagen.

## Kort beskrivning
Bygg en liten mikrosimulering av SKV, genom att använda så mycket
av Spring Framework som möjligt, med hjälp av vad du lärt dig hittills i kursen.
Du ska bygga två applikationer. 

* Den första ska läsa in fiktiv-data och populera en H2 databas. 
* Den andra ska göra en sammanställning av inkomst-uppgifterna, vilka som har 
  över- respektive underskjutande skatt.

# Applikation 1: Populate
Börja med att generera några tusen fiktiva personer med hjälp av
https://mockaroo.com/. Bilden nedan visar hur du ska definiera fälten.

![Mockaroo](./mockaroo.png)

Formler:
* _deductedTax_: `round(salary * (taxPercentage/100.0) * random(0.5,1.5), 0)`
* _personNumber_: `concat(birthYear, pad(birthMonth,2,"0","left"), pad(birthDay,2,"0","left"), "-", checkNums)`

Observera här att vi struntar blankt i "detaljer", såsom att
februari kanske har 31 dagar för någon person.

Välj filformat Custom med semi-kolon som separator.
![File format](./mockaroo-format.png)

Generera den första batch:en med header, men de resterande utan och slå
ihop dem till en stor fil och placera den som en _classpath resource_.

## Class Person
Skapa en klass för person-datat, men skippa beståndsdelarna för
personnumret, dvs `birthYear` till `checkNums`. Säkerställ att person kan 
sorteras i personnummer-ordning, genom 
att implementera `java.lang.Comparable<Person>`. Skriv enhetstest.

## Class PersonCsvMapper
Skapa en klass som kan ta emot en CSV rad och returnera ett person-objekt.
Skriv enhetstest.

## Class PersonLoader
Skapa en klass som via en classpath resource och en csv-mapper, läser in
alla personer och returnerar en `java.util.TreeSet<Person>` med dessa.
Skriv enhetstest, genom att läsa en resource med 10 persdata poster.

## Class PersonPersister
Skapa en klass, som sparar alla personer i en H2 databas. Utgå från koden från
övningarna, dvs klasserna

* `H2DataSourceBuilder`, _JDBC data ska plockas upp från en properties fil_
* `PersonJdbcMapper`
* `JdbcClassicPersonRepo`

Gör sen nödvändiga justeringar och skriv relevanta enhetstest.

Glöm inte att starta H2 servern med shell-scriptet `launch-db-server.sh`.

## Spring XML
Vira ihop denna applikation med Spring XML. Målsättningen här är ett gäng
oberoende klasser med enhetstester, som viras ihop med hjälp av Spring XML.
Detta inkluderar data- och konfigurationsfiler.

# Applikation 2: Audit

Designa en Spring applikation, på liknande sätt som den första, som löper
igenom samtliga personposter i databasen och baserat på personens skatteprocent
(`taxPercentage`) och avdragen skatt (`deductedTax`), räkna fram om denna får
tillbaka på skatten eller restskatt. Uppdatera personposten med resultatet.
Tja, du får lägga till litet grann i personklassen.

Programmet ska skriva ut en sammanställning över

* Antalet med restskatt, samt procentuell andel
* Antalet med skatt tillbaka, samt procentuell andel
* Antalet med varken eller, samt procentuell andel

Glöm inte att målsättningen, även här, är ett gäng oberoende klasser med
enhetstester, vilka viras ihop till en applikation med hjälp av Spring XML.
Reflektera över vilka klasser du behöver, utöver dem du redan har, samt
vilka behöver modifieras.

# Exekvering
Du behöver inte skapa JAR filer, det räcker om du kan köra respektive
program inuti din IDE.


