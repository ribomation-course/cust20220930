package ribomation.tax_simulation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ribomation.tax_simulation.domain.PersonLoader;
import ribomation.tax_simulation.jdbc.PersonPersister;
import ribomation.tax_simulation.util.Timer;

public class PopulateApp {
    public static void main(String[] args) {
        var app = new PopulateApp();
        app.init();
        app.run();
    }

    ApplicationContext ctx;

    void init() {
        try {
            ctx = new ClassPathXmlApplicationContext("/beans-populate.xml");
        } catch (Exception e) {
            if (e.getCause() != null && e.getCause().getMessage() != null) {
                var msg = e.getCause().getMessage();
                if (msg.contains("Connection refused")) {
                    System.out.printf("*** You forgot to launch the database server! ./db/launch-db-server.sh ***%n");
                    System.exit(1);
                }
            }

            e.printStackTrace();
            System.exit(1);
        }
    }

    void run() {
        var timer = new Timer();
        timer.elapsed(() -> {
            var loader = ctx.getBean("personLoader", PersonLoader.class);
            var people = loader.load();

            var persister = ctx.getBean("personPersister", PersonPersister.class);
            persister.insert(people);

            System.out.printf("# rows @ db = %d%n", persister.count());
        });
        System.out.printf("elapsed time %.3f secs%n", timer.elapsedAsSeconds());
    }


}
