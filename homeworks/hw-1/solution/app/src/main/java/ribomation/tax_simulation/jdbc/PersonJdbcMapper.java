package ribomation.tax_simulation.jdbc;

import ribomation.tax_simulation.domain.Person;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PersonJdbcMapper {
    //"personNumber", "firstName", "lastName", "employer", "salary", "taxPercentage", "deductedTax"

    public PreparedStatement prepareInsert(PreparedStatement ps, Person p) {
        try {
            var ix = 1;
            ps.setString(ix++, p.getPersonNumber());
            ps.setString(ix++, p.getFirstName());
            ps.setString(ix++, p.getLastName());
            ps.setString(ix++, p.getEmployer());
            ps.setInt(ix++, p.getSalary());
            ps.setFloat(ix++, p.getTaxPercentage());
            ps.setInt(ix++, p.getDeductedTax());
            return ps;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Person create(ResultSet rs) {
        try {
            var personNumber = rs.getString("personNumber");
            var firstName = rs.getString("firstName");
            var lastName = rs.getString("lastName");
            var employer = rs.getString("employer");
            var salary = rs.getInt("salary");
            var taxPercentage = rs.getFloat("taxPercentage");
            var deductedTax = rs.getInt("deductedTax");
            return new Person(personNumber, firstName, lastName, employer, salary, taxPercentage, deductedTax);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
