package ribomation.tax_simulation.domain;

import java.util.Objects;
import java.util.StringJoiner;

public class Person implements Comparable<Person> {
    private final String personNumber;
    private final String firstName;
    private final String lastName;
    private final String employer;
    private final int salary;
    private final float taxPercentage;
    private int deductedTax;
    private int finalTax = 0;
    private int taxDifference = 0; //+ get back, - must pay, 0 neither

    public Person(String personNumber, String firstName, String lastName, String employer, int salary, float taxPercentage, int deductedTax) {
        this.personNumber = personNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.employer = employer;
        this.salary = salary;
        this.taxPercentage = taxPercentage;
        this.deductedTax = deductedTax;
    }

    public int computeFinalTax() {
        return (int) (salary * taxPercentage / 100);
    }

    public void setFinalTax(int finalTax) {
        this.finalTax = finalTax;
        this.taxDifference = deductedTax - finalTax;
    }

    public boolean hasTaxRest() {
        return taxDifference < 0;
    }

    public boolean hasTaxReturn() {
        return taxDifference > 0;
    }

    public boolean isTaxEven() {
        return taxDifference == 0;
    }

    //testing
    Person(String personNumber) {
        this.personNumber = personNumber;
        this.firstName = "";
        this.lastName = "";
        this.employer = "";
        this.salary = 0;
        this.taxPercentage = 0;
        this.deductedTax = 0;
    }

    //testing
    void setDeductedTax(int deductedTax) {
        this.deductedTax = deductedTax;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Person.class.getSimpleName() + "[", "]")
                .add("personNumber='" + personNumber + "'")
                .add("firstName='" + firstName + "'")
                .add("lastName='" + lastName + "'")
                .add("employer='" + employer + "'")
                .add("salary=" + salary)
                .add("taxPercentage=" + taxPercentage)
                .add("deductedTax=" + deductedTax)
                .add("finalTax=" + finalTax)
                .add("taxDifference=" + taxDifference)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return personNumber.equals(person.personNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(personNumber);
    }

    @Override
    public int compareTo(Person that) {
        return this.personNumber.compareTo(that.personNumber);
    }

    public String getPersonNumber() {
        return personNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmployer() {
        return employer;
    }

    public int getSalary() {
        return salary;
    }

    public float getTaxPercentage() {
        return taxPercentage;
    }

    public int getDeductedTax() {
        return deductedTax;
    }

    public int getFinalTax() {
        return finalTax;
    }

    public int getTaxDifference() {
        return taxDifference;
    }
}
