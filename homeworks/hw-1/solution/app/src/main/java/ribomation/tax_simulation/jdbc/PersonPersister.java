package ribomation.tax_simulation.jdbc;

import ribomation.tax_simulation.domain.Person;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import static java.sql.Statement.RETURN_GENERATED_KEYS;
import static java.util.stream.Collectors.joining;

public class PersonPersister {
    private DataSource dataSource;
    private PersonJdbcMapper jdbcMapper;

    public PersonPersister(DataSource dataSource, PersonJdbcMapper jdbcMapper) {
        this.dataSource = dataSource;
        this.jdbcMapper = jdbcMapper;
    }

    public void createTable() {
        var SQL = """
                DROP TABLE IF EXISTS persons;
                CREATE TABLE persons (
                    id              INTEGER PRIMARY KEY AUTO_INCREMENT,
                    personNumber    CHAR(13),
                    firstName       VARCHAR(64),
                    lastName        VARCHAR(64),
                    employer        VARCHAR(64),
                    salary          INTEGER,
                    taxPercentage   REAL,
                    deductedTax     INTEGER,
                    finalTax        INTEGER NULL DEFAULT 0,
                    taxDifference   INTEGER NULL DEFAULT 0
                );
                        """;

        try (var c = dataSource.getConnection()) {
            try (var s = c.createStatement()) {
                s.execute(SQL);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public int count() {
        try (var c = dataSource.getConnection()) {
            try (var s = c.createStatement()) {
                try (var rs = s.executeQuery("SELECT count(*) FROM persons")) {
                    if (rs.next()) {
                        return rs.getInt(1);
                    } else {
                        return 0;
                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public int insert(Person p) {
        try (var c = dataSource.getConnection()) {
            try (var s = c.prepareStatement(mkInsertSQL(), RETURN_GENERATED_KEYS)) {
                jdbcMapper.prepareInsert(s, p).executeUpdate();
                try (var rs = s.getGeneratedKeys()) {
                    if (rs.next()) return rs.getInt(1);
                    throw new RuntimeException("no generated key");
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void insert(Collection<Person> persons) {
        try (var c = dataSource.getConnection()) {
            try (var s = c.prepareStatement(mkInsertSQL())) {
                for (var p : persons) {
                    jdbcMapper.prepareInsert(s, p).executeUpdate();
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private static String mkInsertSQL() {
        var columns = List.of("personNumber", "firstName", "lastName",
                "employer", "salary", "taxPercentage", "deductedTax");
        var N = columns.stream().collect(joining(","));
        var Q = columns.stream().map(n -> "?").collect(joining(","));
        return "INSERT INTO persons (" + N + ") VALUES (" + Q + ")";
    }

}
