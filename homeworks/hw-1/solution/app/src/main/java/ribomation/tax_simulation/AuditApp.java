package ribomation.tax_simulation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ribomation.tax_simulation.domain.Person;
import ribomation.tax_simulation.jdbc.PersonRetriever;
import ribomation.tax_simulation.util.Timer;

import static java.lang.String.format;

public class AuditApp {
    public static void main(String[] args) {
        var app = new AuditApp();
        app.init();
        app.run();
    }

    ApplicationContext ctx;

    void init() {
        ctx = new ClassPathXmlApplicationContext("/beans-audit.xml");
    }

    void run() {
        final var cnt = new Counts();
        var timer = new Timer();
        timer.elapsed(() -> {
            var retriever = ctx.getBean("personRetriever", PersonRetriever.class);
            var people = retriever.getAll();

            people.forEach(person -> {
                var finalTax = person.computeFinalTax();
                person.setFinalTax(finalTax);
                cnt.update(person);
            });
        });
        System.out.printf("elapsed time %.3f secs%n", timer.elapsedAsSeconds());
        System.out.println(cnt);
    }

    static class Counts {
        int taxRestCount = 0;
        int taxReturnCount = 0;
        int taxEvenCount = 0;

        void update(Person person) {
            if (person.hasTaxRest()) taxRestCount++;
            else if (person.hasTaxReturn()) taxReturnCount++;
            else taxEvenCount++;
        }

        @Override
        public String toString() {
            final var N = taxEvenCount + taxReturnCount + taxRestCount;
            return    format("tax rest  : %4d (%4.1f%%)%n", taxRestCount, 100.0 * taxRestCount / N)
                    + format("tax even  : %4d (%4.1f%%)%n", taxEvenCount, 100.0 * taxEvenCount / N)
                    + format("tax return: %4d (%4.1f%%)%n", taxReturnCount, 100.0 * taxReturnCount / N)
                    + format("population: %4d%n", N)
                    ;
        }

    }

}
