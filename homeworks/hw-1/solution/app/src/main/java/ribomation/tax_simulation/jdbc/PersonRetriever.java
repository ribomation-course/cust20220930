package ribomation.tax_simulation.jdbc;

import ribomation.tax_simulation.domain.Person;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PersonRetriever {
    private DataSource dataSource;
    private PersonJdbcMapper jdbcMapper;

    public PersonRetriever(DataSource dataSource, PersonJdbcMapper jdbcMapper) {
        this.dataSource = dataSource;
        this.jdbcMapper = jdbcMapper;
    }

    public List<Person> getAll() {
        try (var c = dataSource.getConnection()) {
            try (var s = c.createStatement()) {
                var lst = new ArrayList<Person>();
                try (var rs = s.executeQuery("SELECT * FROM persons")) {
                    while (rs.next()) {
                        lst.add(jdbcMapper.create(rs));
                    }
                }
                return lst;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
