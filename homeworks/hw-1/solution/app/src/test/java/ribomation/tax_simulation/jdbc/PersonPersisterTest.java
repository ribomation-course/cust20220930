package ribomation.tax_simulation.jdbc;

import org.junit.jupiter.api.*;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import ribomation.tax_simulation.domain.PersonCsvMapper;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import static java.util.logging.LogManager.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.hamcrest.Matchers.is;

class PersonPersisterTest {
    public static final String PERSONS = "persons";
    PersonPersister persister;
    EmbeddedDatabase db;

    @BeforeAll
    static void init() {
        getLogManager().reset(); //keep quiet
    }

    @BeforeEach
    void setUp() {
        db = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .setName(PERSONS)
                .build();
        persister = new PersonPersister(db, new PersonJdbcMapper());
    }

    @AfterEach
    void tearDown() {
        db.shutdown();
    }

    Optional<String> tableName(Connection c) {
        try (var s = c.createStatement()) {
            try (var rs = s.executeQuery("SHOW TABLES")) {
                if (rs.next()) {
                    return Optional.of(rs.getString(1));
                } else {
                    return Optional.empty();
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    int rowCount(String tbl) {
        try (var c = db.getConnection()) {
            try (var s = c.createStatement()) {
                try (var rs = s.executeQuery("SELECT count(*) FROM " + tbl)) {
                    if (rs.next()) {
                        return rs.getInt(1);
                    } else {
                        return 0;
                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    @DisplayName("creating the domain table, should work")
    void tst_createTable() throws SQLException {
        try (var c = db.getConnection()) {
            assertThat(tableName(c).isEmpty(), is(true));
            persister.createTable();
            assertThat(tableName(c).isPresent(), is(true));
            assertThat(tableName(c).get(), equalToIgnoringCase(PERSONS));
        }
    }

    @Test
    @DisplayName("inserting a single row, should work")
    void tst1_insert() {
        persister.createTable();

        var csv = "Anna;Conda;Bugsify Ltd.;256000;30.0;56000;1997;8;12;6348;19970812-6348";
        var csvMapper = new PersonCsvMapper(";");
        var person = csvMapper.fromCSV(csv);
        var id = persister.insert(person);
        assertThat(id, is(1));
        assertThat(rowCount(PERSONS), is(1));
    }

    @Test
    @DisplayName("inserting a 3 rows, should also work")
    void tst2_insert() {
        persister.createTable();

        var csvMapper = new PersonCsvMapper(";");
        var CSV = List.of(
                "Anna;Conda;Bugsify Ltd.;256000;30.0;56000;1997;8;12;6348;19970812-1111",
                "Per;Silja;Bugsify Ltd.;128000;30.0;28000;1997;8;12;6348;19950812-2222",
                "Justin;Time;Bugsify Ltd.;512000;30.0;12000;1997;8;12;6348;19900812-3333"
        );
        for (var k = 0; k < CSV.size(); ++k) {
            var person = csvMapper.fromCSV(CSV.get(k));
            var id = persister.insert(person);
            assertThat(id, is(k + 1));
            assertThat(rowCount(PERSONS), is(k + 1));
        }
    }

}