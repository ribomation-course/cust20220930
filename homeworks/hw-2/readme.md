# Hemuppgift 2
Färdigställ under vecka 41, före nästa kursdag.
Arbeta gärna tillsammans med en kollega.

----
### [Lösningsförslag hemuppgift 2](./solution/)

----

## Innan du börjar
Gör klart de övningsuppgifter du inte hann med under kursdagen.

## Beskrivning
Modifiera förra veckans två applikationer genom att

* Byta XML mot @nnotation konfiguration
* Bygga om all databaskod till att använda `JdbcTemplate`
* Splittra Audit programmet till två, den ena genomför audit operationen
  och _uppdaterar databasen_, den andra läser bara databasen och gör
  _sammanställningen_ och skriver ut.

Du kan välja om du vill använda _enbart Spring_ eller om
du köra på med _Spring Boot_. 

Om du väljer att generera ett Boot projekt via https://start.spring.io/,
plocka med med 

* Lombok
* Spring Data JDBC
* H2 Database

Kopiera sen över relevanta filer från förra veckans kod, eller
från lösningsförslaget. Förenkla Perosn klassen m.h.a. Lombok.


