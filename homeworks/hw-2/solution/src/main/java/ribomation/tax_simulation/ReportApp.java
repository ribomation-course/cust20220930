package ribomation.tax_simulation;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;
import ribomation.tax_simulation.domain.Summary;
import ribomation.tax_simulation.jdbc.PersonDAO;

@SpringBootApplication
public class ReportApp {
    public static void main(String[] args) {
        SpringApplication.run(ReportApp.class, args);
    }

//    @Bean("report")
//    @DependsOn({"audit"})
//    CommandLineRunner perform(PersonDAO dao) {
//        return args -> {
//            System.out.println("-- Report --");
//            var people = dao.getAll();
//            System.out.printf("# rows: %d%n", people.size());
//
//            var summary = new Summary();
//            people.forEach(summary::update);
//            System.out.println(summary);
//        };
//    }

}
