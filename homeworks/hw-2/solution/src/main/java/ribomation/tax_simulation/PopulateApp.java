package ribomation.tax_simulation;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import ribomation.tax_simulation.domain.PersonLoader;
import ribomation.tax_simulation.jdbc.PersonDAO;

@SpringBootApplication
public class PopulateApp {
    public static void main(String[] args) {
        SpringApplication.run(PopulateApp.class, args);
    }

//    @Bean("populate")
//    CommandLineRunner perform(PersonLoader loader, PersonDAO dao) {
//        return args -> {
//            System.out.println("-- Populate --");
//            loader.setResource(new ClassPathResource("/persons.csv"));
//            var people = loader.load();
//            System.out.printf("# rows @ CSV = %d%n", people.size());
//
//            dao.insert(people);
//            System.out.printf("# rows @ DB  = %d%n", dao.count());
//        };
//    }
}
