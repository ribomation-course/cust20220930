package ribomation.tax_simulation;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.io.ClassPathResource;
import ribomation.tax_simulation.domain.PersonLoader;
import ribomation.tax_simulation.domain.Summary;
import ribomation.tax_simulation.jdbc.PersonDAO;

@SpringBootApplication
public class MainApp {
    public static void main(String[] args) {
        SpringApplication.run(MainApp.class, args);
    }

    @Bean("populate")
    CommandLineRunner populate(PersonLoader loader, PersonDAO dao) {
        return args -> {
            System.out.println("-- Populate --");
            loader.setResource(new ClassPathResource("/persons.csv"));
            var people = loader.load();
            System.out.printf("# rows @ CSV = %d%n", people.size());

            dao.insert(people);
            System.out.printf("# rows @ DB  = %d%n", dao.count());
        };
    }

    @Bean("audit")
    @DependsOn({"populate"})
    CommandLineRunner audit(PersonDAO dao) {
        return args -> {
            System.out.println("-- Audit --");
            var people = dao.getAll();
            System.out.printf("# rows: %d%n", people.size());

            people.forEach(person -> {
                var finalTax = person.computeFinalTax();
                person.setFinalTax(finalTax);
            });

            dao.update(people);
        };
    }

    @Bean("report")
    @DependsOn({"audit"})
    CommandLineRunner report(PersonDAO dao) {
        return args -> {
            System.out.println("-- Report --");
            var people = dao.getAll();
            System.out.printf("# rows: %d%n", people.size());

            var summary = new Summary();
            people.forEach(summary::update);
            System.out.println(summary);
        };
    }

}
