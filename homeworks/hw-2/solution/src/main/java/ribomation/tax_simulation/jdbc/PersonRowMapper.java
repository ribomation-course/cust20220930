package ribomation.tax_simulation.jdbc;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import ribomation.tax_simulation.domain.Person;

import java.sql.ResultSet;
import java.sql.SQLException;

@Service
public class PersonRowMapper implements RowMapper<Person> {

    @Override
    public Person mapRow(ResultSet rs, int rowNum) throws SQLException {
        var id = rs.getInt("id");
        var personNumber = rs.getString("personNumber");
        var firstName = rs.getString("firstName");
        var lastName = rs.getString("lastName");
        var employer = rs.getString("employer");
        var salary = rs.getInt("salary");
        var taxPercentage = rs.getFloat("taxPercentage");
        var deductedTax = rs.getInt("deductedTax");
        var finalTax = rs.getInt("finalTax");
        var taxDifference = rs.getInt("taxDifference");

        return Person.of(id,
                personNumber, firstName, lastName,
                employer, salary,
                taxPercentage, deductedTax, finalTax, taxDifference);
    }

}
