package ribomation.tax_simulation.util;

public class Timer {
    long elapsedNanoSecs = 0;

    public double elapsedAsSeconds() {
        return elapsedNanoSecs * 1E-9;
    }

    public void elapsed(Runnable stmts) {
        var startTime = System.nanoTime();
        try {
            stmts.run();
        } finally {
            var endTime = System.nanoTime();
            elapsedNanoSecs = endTime - startTime;
        }
    }
}
