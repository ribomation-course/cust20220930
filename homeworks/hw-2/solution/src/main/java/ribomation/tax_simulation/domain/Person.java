package ribomation.tax_simulation.domain;

import lombok.*;

@ToString
@EqualsAndHashCode
@RequiredArgsConstructor(staticName = "of")
@AllArgsConstructor(staticName = "of")
public class Person implements Comparable<Person> {
    @Getter @Setter private int id;
    @Getter
    private final String personNumber;
    @Getter
    private final String firstName;
    @Getter
    private final String lastName;
    @Getter
    private final String employer;
    @Getter
    private final int salary;
    @Getter
    private final float taxPercentage;
    @Getter
    private final int deductedTax;
    @Getter
    private int finalTax = 0;
    @Getter
    private int taxDifference = 0; //+ get back, - must pay, 0 neither

    public int computeFinalTax() {
        return (int) (salary * taxPercentage / 100);
    }

    public void setFinalTax(int finalTax) {
        this.finalTax = finalTax;
        this.taxDifference = deductedTax - finalTax;
    }

    public boolean hasTaxRest() {
        return taxDifference < 0;
    }

    public boolean hasTaxReturn() {
        return taxDifference > 0;
    }

    public boolean isTaxEven() {
        return taxDifference == 0;
    }

    @Override
    public int compareTo(Person that) {
        return this.personNumber.compareTo(that.personNumber);
    }

    //testing methods

    static Person of(String personNumber) {
        return new Person(personNumber, "", "", "", 0, 0, 0);
    }

    static Person of(int deductedTax) {
        return new Person(-1, "", "", "", "", 0, 0, deductedTax, 0, 0);
    }

}
