package ribomation.tax_simulation.domain;

import org.springframework.stereotype.Component;

@Component
public class PersonCsvMapper {
    private final String delim;

    public PersonCsvMapper() {
        this(";");
    }

    public PersonCsvMapper(String delim) {
        this.delim = delim;
    }

    public Person fromCSV(String csv) {
        var f = csv.split(delim);
        var k = 0;

        //first_name;last_name;employer;salary;taxPercentage;deductedTax;birthYear;birthMonth;birthDay;checkNums;personNumber
        var first_name = f[k++];
        var last_name = f[k++];
        var employer = f[k++];
        var salary = Integer.parseInt(f[k++]);
        var taxPercentage = Float.parseFloat(f[k++]);
        var deductedTax = Integer.parseInt(f[k++]);
        var birthYear = f[k++];
        var birthMonth = f[k++];
        var birthDay = f[k++];
        var checkNums = f[k++];
        var personNumber = f[k++];

        return  Person.of(personNumber, first_name, last_name, employer, salary, taxPercentage, deductedTax);
    }
}
