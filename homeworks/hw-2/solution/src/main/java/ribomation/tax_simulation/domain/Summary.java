package ribomation.tax_simulation.domain;

import static java.lang.String.format;

public class Summary {
    private int taxRestCount = 0;
    private int taxReturnCount = 0;
    private int taxEvenCount = 0;

    public void update(Person person) {
        if (person.hasTaxRest()) taxRestCount++;
        else if (person.hasTaxReturn()) taxReturnCount++;
        else taxEvenCount++;
    }

    @Override
    public String toString() {
        final var N = taxEvenCount + taxReturnCount + taxRestCount;
        return format("tax rest  : %4d (%4.1f%%)%n", taxRestCount, 100.0 * taxRestCount / N)
                + format("tax even  : %4d (%4.1f%%)%n", taxEvenCount, 100.0 * taxEvenCount / N)
                + format("tax return: %4d (%4.1f%%)%n", taxReturnCount, 100.0 * taxReturnCount / N)
                + format("population: %4d%n", N)
                ;
    }
}
