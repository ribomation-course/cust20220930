package ribomation.tax_simulation.jdbc;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ribomation.tax_simulation.domain.Person;

import java.util.Collection;
import java.util.List;

import static java.lang.String.format;
import static java.util.stream.Collectors.joining;

@Repository
public class PersonDAO implements InitializingBean {
    private final JdbcTemplate jdbc;
    private final PersonRowMapper mapper;

    public PersonDAO(JdbcTemplate jdbc, PersonRowMapper mapper) {
        this.jdbc = jdbc;
        this.mapper = mapper;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        createTable();
    }

    public void createTable() {
        jdbc.execute(TABLE_SQL);
    }

    public int count() {
        final String sql = "SELECT count(*) FROM " + TABLE;
        return jdbc.queryForObject(sql, Integer.class);
    }

    public List<Person> getAll() {
        final String sql = "SELECT * FROM " + TABLE;
        return jdbc.query(sql, mapper);
    }

    public void insert(Collection<Person> persons) {
        final var sql = mkInsertSQL();
        final var batchSize = 1000;
        jdbc.batchUpdate(sql, persons, batchSize, (ps, p) -> {
            int idx = 1;
            ps.setString(idx++, p.getPersonNumber());
            ps.setString(idx++, p.getFirstName());
            ps.setString(idx++, p.getLastName());
            ps.setString(idx++, p.getEmployer());
            ps.setInt(idx++,    p.getSalary());
            ps.setFloat(idx++,  p.getTaxPercentage());
            ps.setInt(idx++,    p.getDeductedTax());
        });
    }

    private static String mkInsertSQL() {
        var columns = List.of("personNumber", "firstName", "lastName",
                "employer", "salary", "taxPercentage", "deductedTax");
        var N = columns.stream().collect(joining(","));
        var Q = columns.stream().map(n -> "?").collect(joining(","));
        return "INSERT INTO persons (" + N + ") VALUES (" + Q + ")";
    }


    public void update(Collection<Person> persons) {
        final var sql = mkUpdateSQL();
        final var batchSize = 1000;
        jdbc.batchUpdate(sql, persons, batchSize, (ps, p) -> {
            int idx = 1;
            ps.setString(idx++, p.getPersonNumber());
            ps.setString(idx++, p.getFirstName());
            ps.setString(idx++, p.getLastName());
            ps.setString(idx++, p.getEmployer());
            ps.setInt(idx++,    p.getSalary());
            ps.setFloat(idx++,  p.getTaxPercentage());
            ps.setInt(idx++,    p.getDeductedTax());
            ps.setInt(idx++,    p.getFinalTax());
            ps.setInt(idx++,    p.getTaxDifference());
            ps.setInt(idx++,    p.getId());
        });
    }

    private static String mkUpdateSQL() {
        var pk = "id";
        var columns = List.of("personNumber", "firstName", "lastName",
                "employer", "salary",
                "taxPercentage", "deductedTax", "finalTax", "taxDifference");
        var setters = columns.stream()
                .map(c -> format("%s = ?", c))
                .collect(joining(", "));
        return format("UPDATE %s SET %s WHERE %s = ?", TABLE, setters, pk);
    }



    static final String TABLE = "persons";
    static final String TABLE_SQL = """
            DROP TABLE IF EXISTS persons;
            CREATE TABLE persons (
                id              INTEGER PRIMARY KEY AUTO_INCREMENT,
                personNumber    CHAR(13),
                firstName       VARCHAR(64),
                lastName        VARCHAR(64),
                employer        VARCHAR(64),
                salary          INTEGER,
                taxPercentage   REAL,
                deductedTax     INTEGER,
                finalTax        INTEGER NULL DEFAULT 0,
                taxDifference   INTEGER NULL DEFAULT 0
            );""";
}
