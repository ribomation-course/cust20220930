package ribomation.tax_simulation;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;
import ribomation.tax_simulation.jdbc.PersonDAO;

@SpringBootApplication
public class AuditApp {
    public static void main(String[] args) {
        SpringApplication.run(AuditApp.class, args);
    }

//    @Bean("audit")
//    @DependsOn({"populate"})
//    CommandLineRunner perform(PersonDAO dao) {
//        return args -> {
//            System.out.println("-- Audit --");
//            var people = dao.getAll();
//            System.out.printf("# rows: %d%n", people.size());
//
//            people.forEach(person -> {
//                var finalTax = person.computeFinalTax();
//                person.setFinalTax(finalTax);
//            });
//
//            dao.update(people);
//        };
//    }

}
