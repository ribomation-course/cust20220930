package ribomation.tax_simulation.domain;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

//@SpringBootTest
class PersonTest {

    @Test
    @DisplayName("when final-tax is greater than deducted, then has to pay")
    void tst1_setFinalTax() {
        var p = Person.of(1000);
        p.setFinalTax(1100);
        assertThat(p.getFinalTax(), is(1100));
        assertThat(p.getTaxDifference(), lessThan(0));
    }

    @Test
    @DisplayName("when final-tax is lower than deducted, then will get back")
    void tst2_setFinalTax() {
        var p = Person.of(1000);
        p.setFinalTax(900);
        assertThat(p.getFinalTax(), is(900));
        assertThat(p.getTaxDifference(), greaterThan(0));
    }

    @Test
    @DisplayName("sorting should work")
    void compareTo() {
        var persons = List.of(
                Person.of("19980501-1111"),
                Person.of("19990501-1111"),
                Person.of("19950501-1111"),
                Person.of("19960501-1111")
        );
        var sorted = persons.stream().sorted().toList();
        assertThat(sorted.get(0).getPersonNumber(), is("19950501-1111"));
        assertThat(sorted.get(3).getPersonNumber(), is("19990501-1111"));
    }

}
