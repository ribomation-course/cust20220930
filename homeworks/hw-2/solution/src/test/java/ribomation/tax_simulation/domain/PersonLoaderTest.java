package ribomation.tax_simulation.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

class PersonLoaderTest {
    PersonLoader loader;

    @BeforeEach
    void setUp() {
        loader = new PersonLoader();
        loader.setMapper(new PersonCsvMapper(";"));
    }

    @Test
    @DisplayName("setting an existing resource should work")
    void tst1_setResource() {
        Resource r = new ClassPathResource("some-persons.csv");
        loader.setResource(r);
    }

    @Test
    @DisplayName("setting an existing resource, but not *.csv should throw")
    void tst2_setResource() {
        var x= assertThrows(IllegalArgumentException.class, () -> {
            Resource r = new ClassPathResource("some-persons.xml");
            loader.setResource(r);
        });
        assertThat(x.getMessage(), containsString("not a CSV"));
    }

    @Test
    @DisplayName("setting an non-existing resource, should throw")
    void tst3_setResource() {
        var x= assertThrows(IllegalArgumentException.class, () -> {
            Resource r = new ClassPathResource("no-such-file.txt");
            loader.setResource(r);
        });
        assertThat(x.getMessage(), containsString("cannot read"));
    }

    @Test
    @DisplayName("loading 10 persons, should work")
    void tst1_load() {
        loader.setResource(new ClassPathResource("some-persons.csv"));
        var result = loader.load();
        assertThat(result, notNullValue());
        assertThat(result.isEmpty(), not(true));
        assertThat(result.size(), is(10));
        assertThat(result.first().getPersonNumber(), is("19330308-6819"));
        assertThat(result.last().getPersonNumber(), is("19991116-9355"));
    }

}
