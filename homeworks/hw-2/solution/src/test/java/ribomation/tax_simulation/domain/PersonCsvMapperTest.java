package ribomation.tax_simulation.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

class PersonCsvMapperTest {
    final String sampleCSV = "Östen;Skinley;Fivespan;159492;32.2;75278;1974;10;1;9997;19741001-9997";
    PersonCsvMapper mapper;

    @BeforeEach
    void init() {
        mapper = new PersonCsvMapper(";");
    }

    @Test
    @DisplayName("parsing csv line should work")
    void tst1_fromCSV() {
        var p = mapper.fromCSV(sampleCSV);
        assertThat(p, notNullValue());
        assertThat(p.getFirstName(), is("Östen"));
        assertThat(p.getPersonNumber(), is("19741001-9997"));
        assertThat(p.getSalary(), is(159492));
    }

    @Test
    @DisplayName("parsing invalid csv should throw")
    void tst2_fromCSV() {
        var x = assertThrows(NumberFormatException.class, () -> {
            mapper.fromCSV(sampleCSV.replace("159492", "XYZ"));
        });
        assertThat(x.getMessage(), is("For input string: \"XYZ\""));
    }

}
