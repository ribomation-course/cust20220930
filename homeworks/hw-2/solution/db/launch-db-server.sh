#!/usr/bin/env bash
set -eux

DB_DIR=`dirname "$0"`
DATA_DIR=$DB_DIR/data
H2_JAR=$DB_DIR/lib/h2.jar
H2_CLASS=org.h2.tools.Server

java -cp $H2_JAR $H2_CLASS -tcp -baseDir $DATA_DIR -ifNotExists
