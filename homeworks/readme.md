# Hemuppgifter

Utför respektive uppgift mellan kursdagarna.

* [Uppgift 1, deadline 7 okt](./hw-1/readme.md) - [Lösningsförslag](./hw-1/solution/)
* [Uppgift 2, deadline 14 okt](./hw-2/readme.md) - [Lösningsförslag](./hw-2/solution/)
* [Uppgift 3, deadline 21 okt](./hw-3/readme.md) - [Lösningsförslag](./hw-3/solution/)


