package ribomation.tax_simulation_web.web;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import java.util.Date;

import static java.lang.String.format;

@Service
@ConfigurationProperties(prefix = "app")
@Data
public class AppInfo {
    String name;
    String version;
    String date= format("%tF", new Date());
    String year = format("%tY", new Date());
}
