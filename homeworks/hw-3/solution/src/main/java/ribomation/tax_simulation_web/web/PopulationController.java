package ribomation.tax_simulation_web.web;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ribomation.tax_simulation_web.database.PersonDAO;
import ribomation.tax_simulation_web.domain.Person;

@Controller
@RequestMapping("/population")
@Log
public class PopulationController {
    @Autowired
    PersonDAO dao;
    @Autowired
    AppInfo info;

    @GetMapping
    public String index(Model m,
                        @RequestParam(name = "nav", required = false, defaultValue = "curr") String nav,
                        @RequestParam(name = "page", required = false, defaultValue = "1") int page
    ) {
        final var pageSize = 10;
        var pageable = PageRequest.of(page <= 0 ? 0 : page - 1, pageSize);
        var navigation = switch (nav) {
            case "curr" -> pageable;
            case "first" -> pageable.first();
            case "last" -> pageable.withPage(page - 1);
            case "next" -> pageable.next();
            case "prev" -> pageable.previousOrFirst();
            default -> pageable;
        };
        var result = dao.findAll(navigation);

        var lastPageNumber = result.getTotalPages();
        if (navigation.getPageNumber() > (lastPageNumber - 1)) {
            navigation = pageable.withPage(lastPageNumber - 1);
            result = dao.findAll(navigation);
        }

        m.addAttribute("info", info);
        m.addAttribute("items", result.getContent());
        m.addAttribute("page", navigation.getPageNumber() + 1);
        m.addAttribute("last", lastPageNumber);
        m.addAttribute("totalPages", result.getTotalPages());

        return "population/index";
    }

}
