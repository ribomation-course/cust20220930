package ribomation.tax_simulation_web.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;

import static java.lang.String.format;

//@Data
//@NoArgsConstructor
public class Summary {
    public int taxRestCount = 0;
    public double taxRestFraction = 0;
    public int taxReturnCount = 0;
    public double taxReturnFraction = 0;
    public int taxEvenCount = 0;
    public double taxEvenFraction = 0;
    public int population = 0;

    public void update(Person person) {
        if (person.hasTaxRest()) ++taxRestCount;
        else if (person.hasTaxReturn()) ++taxReturnCount;
        else ++taxEvenCount;
    }

    public void finish() {
        population = taxRestCount + taxEvenCount + taxReturnCount;
        taxRestFraction = 100.0 * taxRestCount / population;
        taxEvenFraction = 100.0 * taxEvenCount / population;
        taxReturnFraction = 100.0 * taxReturnCount / population;
    }

    @Override
    public String toString() {
        final var N = population;
        return format("tax rest  : %4d (%4.1f%%)%n", taxRestCount, taxRestFraction)
                + format("tax even  : %4d (%4.1f%%)%n", taxEvenCount, taxEvenFraction)
                + format("tax return: %4d (%4.1f%%)%n", taxReturnCount, taxReturnFraction)
                + format("population: %4d", population)
                ;
    }
}
