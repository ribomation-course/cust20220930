package ribomation.tax_simulation_web.web;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;

@Controller
@RequestMapping("/")
@Log
public class HomeController {
    @Autowired AppInfo info;

    @GetMapping
    public String index(Model m) {
        info.date = String.format("%1$tF %1$tT", new Date());
        m.addAttribute("info", info);
        log.info("info: " + info);

        return "home/index";
    }

}
