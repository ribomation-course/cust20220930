package ribomation.tax_simulation_web.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.TreeSet;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

@Service
public class PersonLoader {
    private Resource resource;
    private PersonCsvMapper mapper;

    public void setResource(Resource resource) {
        this.resource = resource;
        if (!this.resource.isReadable()) {
            throw new IllegalArgumentException("cannot read from: " + this.resource.getFilename());
        }
        if (!requireNonNull(this.resource.getFilename()).endsWith(".csv")) {
            throw new IllegalArgumentException("not a CSV file: " + this.resource.getFilename());
        }
    }

    @Autowired
    public void setMapper(PersonCsvMapper mapper) {
        this.mapper = mapper;
    }

    public TreeSet<Person> load() {
        try (var in = new BufferedReader(
                new InputStreamReader(
                        resource.getInputStream()))) {
            return in.lines()
                    .skip(1) //omit header
                    .map(csv -> mapper.fromCSV(csv))
                    .collect(Collectors.toCollection(TreeSet::new));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
