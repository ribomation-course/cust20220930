package ribomation.tax_simulation_web.web;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ribomation.tax_simulation_web.database.PersonDAO;
import ribomation.tax_simulation_web.domain.Summary;
import ribomation.tax_simulation_web.util.Timer;

@Controller
@RequestMapping("/summary")
@Log
public class SummaryController {
    @Autowired PersonDAO dao;
    @Autowired AppInfo info;

    @GetMapping
    public String index(Model m) {
        var t = new Timer("Summary");
        var summary = new Summary();
        t.elapsed(() -> {
            dao.findAll().forEach(summary::update);
        });
        summary.finish();

        m.addAttribute("summary", summary);
        m.addAttribute("elapsed", t.toString());
        m.addAttribute("info", info);
        log.info(info.toString());

        return "summary/index";
    }

}
