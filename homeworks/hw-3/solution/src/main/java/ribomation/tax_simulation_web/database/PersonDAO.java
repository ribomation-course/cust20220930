package ribomation.tax_simulation_web.database;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ribomation.tax_simulation_web.domain.Person;

@Repository
public interface PersonDAO extends PagingAndSortingRepository<Person, Integer> {

}
