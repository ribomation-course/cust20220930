package ribomation.tax_simulation_web.domain;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@ToString
@EqualsAndHashCode
@RequiredArgsConstructor(staticName = "of")
@AllArgsConstructor(staticName = "of")
//@NoArgsConstructor
public class Person implements Comparable<Person> {
    @Getter
    @Setter
    @Id
    @GeneratedValue
    private int id;

    @Getter
    private String personNumber;
    @Getter
    private String firstName;
    @Getter
    private String lastName;
    @Getter
    private String employer;
    @Getter
    private int salary;
    @Getter
    private float taxPercentage;
    @Getter
    private int deductedTax;
    @Getter
    private int finalTax = 0;
    @Getter
    private int taxDifference = 0; //+ get back, - must pay, 0 neither



    public int computeFinalTax() {
        return (int) (salary * taxPercentage / 100);
    }

    public void setFinalTax(int finalTax) {
        this.finalTax = finalTax;
        this.taxDifference = deductedTax - finalTax;
    }

    public boolean hasTaxRest() {
        return taxDifference < 0;
    }

    public boolean hasTaxReturn() {
        return taxDifference > 0;
    }

    public boolean isTaxEven() {
        return taxDifference == 0;
    }

    @Override
    public int compareTo(Person that) {
        return this.personNumber.compareTo(that.personNumber);
    }

    public static Person of(String personNumber, String firstName, String lastName, String employer, int salary, float taxPercentage, int deductedTax) {
        return Person.of(-1, personNumber, firstName, lastName, employer, salary, taxPercentage, deductedTax, 0, 0);
    }

    static Person of(String personNumber) {
        return Person.of(-1, personNumber, "", "", "", 0, 0, 0, 0, 0);
    }

    static Person of(int deductedTax) {
        return new Person(-1, "", "", "", "", 0, 0, deductedTax, 0, 0);
    }

}
