package ribomation.tax_simulation_web.util;

public class Timer {
    private final String name;
    private long elapsedNanoSecs = 0;

    public Timer(String name) {
        this.name = name;
    }

    public double elapsedAsSeconds() {
        return elapsedNanoSecs * 1E-9;
    }

    public void elapsed(Runnable stmts) {
        var startTime = System.nanoTime();
        try {
            stmts.run();
        } finally {
            var endTime = System.nanoTime();
            elapsedNanoSecs = endTime - startTime;
        }
    }

    @Override
    public String toString() {
        return String.format("%s: elapsed %.2f seconds", name, elapsedAsSeconds());
    }
}
