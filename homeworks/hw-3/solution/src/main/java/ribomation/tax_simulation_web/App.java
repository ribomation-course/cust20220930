package ribomation.tax_simulation_web;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.io.ClassPathResource;
import ribomation.tax_simulation_web.database.PersonDAO;
import ribomation.tax_simulation_web.domain.PersonLoader;
import ribomation.tax_simulation_web.domain.Summary;
import ribomation.tax_simulation_web.util.Timer;

@SpringBootApplication
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @Bean("populate")
    CommandLineRunner populate(PersonLoader loader, PersonDAO dao) {
        return args -> {
            System.out.println("-- Populate --");
            var t = new Timer("Populate");
            t.elapsed(() -> {
                loader.setResource(new ClassPathResource("/persons.csv"));
                var people = loader.load();
                System.out.printf("# rows @ CSV = %d%n", people.size());

                dao.saveAll(people);
                System.out.printf("# rows @ DB  = %d%n", dao.count());
            });
            System.out.println(t);
        };
    }

    @Bean("audit")
    @DependsOn({"populate"})
    CommandLineRunner audit(PersonDAO dao) {
        return args -> {
            System.out.println("-- Audit --");
            var t = new Timer("Audit");
            t.elapsed(() -> {
                var people = dao.findAll();

                people.forEach(person -> {
                    var finalTax = person.computeFinalTax();
                    person.setFinalTax(finalTax);
                });

                dao.saveAll(people);
                System.out.printf("# rows updated = %d%n", dao.count());
            });
            System.out.println(t);
        };
    }

    @Bean("report")
    @DependsOn({"audit"})
    CommandLineRunner report(PersonDAO dao) {
        return args -> {
            System.out.println("-- Report --");
            var t = new Timer("Report");
            t.elapsed(() -> {
                var summary = new Summary();
                dao.findAll().forEach(summary::update);
                summary.finish();
                System.out.println(summary);
            });
            System.out.println(t);
        };
    }

}
