# Hemuppgift 3
Färdigställ under vecka 42, före nästa kursdag.
Arbeta gärna tillsammans med en kollega.

----
### [Lösningsförslag hemuppgift 3](./solution/)

----

## Innan du börjar
Gör klart de övningsuppgifter du inte hann med under kursdagen.

## Beskrivning
Utöka förra veckans applikationer 

* Baserad på Spring Boot (om du inte redan gjorde i HW2)
* Bygg om databas-koden till att nu använda JPA
* Skapa en enkel _klassisk MPA_ webapp med Spring MVC och _Mustache_ template engine,
  som utför det som den förra veckans tredje applikation gjorde, dvs visa en 
  sammanställning hämtat från databasen

## Har du tid över 
Så, bygger en du webb-sida som visar en tabell med personerna
i databasen. 

Du kan inte visa alla på en gång, eftersom det skulle bli
för mycket. Välj t.ex. 30 rader i tager och att man kan navigera genom
att bläddra till nästa sida eller gå tillbaka.

Basera ditt DAO interface på `PagingAndSortingRepository`.
Här är en artikel-länk som visar ett konkret exempel
https://bushansirgur.in/spring-data-jpa-pagination-with-example/



