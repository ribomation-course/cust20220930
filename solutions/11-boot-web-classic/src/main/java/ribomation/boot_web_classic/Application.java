package ribomation.boot_web_classic;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import ribomation.boot_web_classic.domain.PersonDAO;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    CommandLineRunner populate(PersonDAO dao) {
        return args -> {
            dao.populate();
            System.out.printf("populated %d objs%n", dao.count());
        };
    }

}
