package ribomation.boot_web_classic.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ribomation.boot_web_classic.domain.Person;
import ribomation.boot_web_classic.domain.PersonDAO;

@Controller
@RequestMapping("/")
public class PersonController {
    @Autowired
    private PersonDAO dao;

    @GetMapping
    public String index() {
        return "redirect:/list";
    }

    @GetMapping("list")
    public String list(Model data) {
        var lst = dao.findAll();
        data.addAttribute("persons", lst);
        return "list";
    }

    @GetMapping("show/{id}")
    public String show(@PathVariable int id, Model data) {
        var obj = dao.findById(id);
        if (obj.isPresent()) {
            data.addAttribute("person", obj.get());
            return "show";
        }
        return "redirect:/list";
    }

    @GetMapping("create")
    public String create(Model data) {
        data.addAttribute("person", dao.create());
        data.addAttribute("action", "Create new person");
        return "edit";
    }

    @GetMapping("edit/{id}")
    public String edit(@PathVariable int id, Model data) {
        var obj = dao.findById(id);
        if (obj.isEmpty()) {
            throw new RuntimeException("not found");
        }
        data.addAttribute("person", obj.get());
        data.addAttribute("action", "Edit person with id=" + id);
        return "edit";
    }

    @PostMapping("save/{id}")
    public String save(@PathVariable int id, Person delta) {
        if (id == -1) {
            var p = dao.create();
            dao.insert(p);
            dao.update(p.getId(), delta);
            return "redirect:/show/" + p.getId();
        } else {
            if (dao.existsById(id)) {
                dao.update(id, delta);
            } else {
                throw new RuntimeException("not found");
            }
            return "redirect:/show/" + id;
        }
    }

    @GetMapping("remove/{id}")
    public String remove(@PathVariable int id, Model data) {
        var obj = dao.findById(id);
        if (obj.isEmpty()) {
            throw new RuntimeException("not found");
        }
        dao.delete(obj.get().getId());
        return "redirect:/";
    }

}
