package ribomation.boot_web_classic.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
public class Person {
	private Integer id;
    private String name;
    private Integer age;
    private String avatar;
    private String company;
    private String email;
}
