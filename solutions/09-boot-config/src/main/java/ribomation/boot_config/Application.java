package ribomation.boot_config;

import lombok.Data;
import lombok.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Service
    @ConfigurationProperties("ribomation")
    @Data
    static class RibomationProps {
        String name;
        int version;
        boolean silly;
    }

    @Bean
    CommandLineRunner doit(Environment env, RibomationProps props) {
        return args -> {
            System.out.printf("banner: %s%n", env.getProperty("spring.main.banner-mode", "??"));
            System.out.printf("name: %s%n", env.getProperty("ribomation.name", "??"));
            System.out.printf("props: %s%n", props);
        };
    }
}
