package ribomation.bootjpa.domain;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonJsonMapper {
    private final Gson gson;

    public PersonJsonMapper() {
        gson = new GsonBuilder()
                //.setPrettyPrinting()
                .create();
    }

    public String toJson(Person person) {
        return gson.toJson(person);
    }

    public String toJson(List<Person> persons) {
        return gson.toJson(persons);
    }

}
