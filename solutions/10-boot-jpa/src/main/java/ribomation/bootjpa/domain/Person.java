package ribomation.bootjpa.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@Entity
public class Person {
    @Id @GeneratedValue
    private int id;

    private String name;
    private int age;
    private boolean female;
    private int postCode;

    public static Person of(String name, String age, String gender, String postCode) {
        return of(-1, name, age, gender, postCode);
    }

    public static Person of(int id, String name, String age, String gender, String postCode) {
        return of(id, name, Integer.parseInt(age), gender.equals("Female"), Integer.parseInt(postCode));
    }

}
