package ribomation.bootjpa.domain;

import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class PersonLoader {
    private final Resource csvResource;
    private final PersonCsvMapper mapper;

    public PersonLoader(Resource csvResource, PersonCsvMapper mapper) {
        this.csvResource = csvResource;
        this.mapper = mapper;
    }

    public Collection<Person> loadAll() {
        try {
            return loadAll(csvResource.getInputStream());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public Collection<Person> loadAll(InputStream is)  {
        try (var in = new BufferedReader(new InputStreamReader(is))) {
            return in.lines()
                    .skip(1)
                    .map(mapper::fromCSV)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
