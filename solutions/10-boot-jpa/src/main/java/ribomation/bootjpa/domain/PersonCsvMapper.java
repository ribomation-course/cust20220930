package ribomation.bootjpa.domain;

import org.springframework.stereotype.Service;

@Service
public class PersonCsvMapper {
    public String defaultDelim = ";";

    public PersonCsvMapper() {
    }

    public PersonCsvMapper(String defaultDelim) {
        this.defaultDelim = defaultDelim;
    }

    public Person fromCSV(String csv) {
        return fromCSV(csv, defaultDelim);
    }

    public Person fromCSV(String csv, String delim) {
        var f = csv.split(delim);
        var ix = 0;

        //name;age;gender;postCode
        var name = f[ix++];
        var age = f[ix++];
        var gender = f[ix++];
        var postCode = f[ix++];

        try {
            return Person.of(-1, name, age, gender, postCode);
        } catch (NumberFormatException e) {
            System.out.printf("invalid: %s%n", csv);
            return Person.of(-1, "", 0, false, 0);
        }
    }

}
