package ribomation.bootjpa;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import ribomation.bootjpa.database.PersonDAO;
import ribomation.bootjpa.domain.Person;
import ribomation.bootjpa.domain.PersonJsonMapper;
import ribomation.bootjpa.domain.PersonLoader;

@SpringBootApplication
public class App {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @Bean
    Resource csvFile() {
        return new ClassPathResource("/persons.orig.csv");
    }

    @Bean
    CommandLineRunner useCase(PersonLoader loader, PersonDAO dao, PersonJsonMapper jsonMapper) {
        return args -> {
            var objs = loader.loadAll();
            System.out.printf("Loaded %d rows from CSV%n", objs.size());

            dao.saveAll(objs);
            System.out.printf("Stored %d rows in DB%n", dao.count());

            var selection = dao.findByAgeBetweenAndPostCodeLessThanAndFemale(30, 40, 12_500, true);
            System.out.printf("Found %d persons%n", selection.size());
            System.out.printf("JSON: %s%n", jsonMapper.toJson(selection));

            dao.deleteAll(selection);
            System.out.printf("Removed %d persons%n", selection.size());
            System.out.printf("Reduced size to %d persons%n", dao.count());

            var onePerson = dao.findById(1).orElseThrow();
            System.out.printf("Found: %s%n", onePerson);

            onePerson.setName("Per Silja");
            onePerson.setAge(42);
            onePerson.setFemale(false);
            onePerson.setPostCode(25_500);
            dao.save(onePerson);
            System.out.printf("Modified: %s%n", dao.findById(1).orElseThrow());
        };
    }

}
