package ribomation;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ribomation.domain.PersonCsvMapper;
import ribomation.domain.PersonJsonMapper;
import ribomation.domain.PersonLoader;
import ribomation.domain.PersonRepo;
import ribomation.jdbc.H2DataSourceBuilder;
import ribomation.jdbc.JdbcClassicPersonRepo;
import ribomation.jdbc.PersonJdbcMapper;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;

@Configuration
public class App {
    public static void main(String[] args) throws Exception {
        var app = new App();
//        app.run();
//        app.runXML();
        app.runJavaCfg();
    }

    void runJavaCfg() {
        var ctx = new AnnotationConfigApplicationContext(App.class);

        var csvResource = ctx.getBean("csvResource", InputStream.class);
        var loader      = ctx.getBean("loader", PersonLoader.class);
        var jsonMapper  = ctx.getBean("jsonMapper", PersonJsonMapper.class);
        var dao         = ctx.getBean("dao", PersonRepo.class);

        var useCase     = new UseCase(csvResource, loader, dao, jsonMapper);
        useCase.run();
    }

    @Bean
    InputStream csvResource() {
        return getClass().getResourceAsStream("/persons.csv");
    }

    @Bean
    PersonLoader loader() {
        var loader = new PersonLoader();
        loader.setMapper(csvMapper());
        return loader;
    }

    @Bean
    PersonCsvMapper csvMapper() {
        return new PersonCsvMapper(";");
    }

    @Bean
    PersonJsonMapper jsonMapper() {
        return new PersonJsonMapper();
    }

    @Bean
    PersonRepo dao() {
        var repo = new JdbcClassicPersonRepo();
        repo.setMapper(jdbcMapper());
        repo.setDataSource(dataSource());
        repo.createTable();
        return repo;
    }

    @Bean
    PersonJdbcMapper jdbcMapper() {
        return new PersonJdbcMapper();
    }

    @Bean
    Connection dataSource() {
        return new H2DataSourceBuilder().openServer();
    }

/*
    void runXML() throws IOException {
        var ctx         = new ClassPathXmlApplicationContext("/beans.xml");

        var csvResource = ctx.getResource("classpath:/persons.csv").getInputStream();
        var loader      = ctx.getBean("loader", PersonLoader.class);
        var dao         = ctx.getBean("dao", PersonRepo.class);
        var jsonMapper  = ctx.getBean("jsonMapper", PersonJsonMapper.class);

        var useCase     = new UseCase(csvResource, loader, dao, jsonMapper);
        useCase.run();
    }
*/

/*

    void run() {
        var csvResource = getCsvResource();
        var loader = getPersonLoader();
        var dao = getPersonRepo();
        var jsonMapper = getJsonMapper();

        var useCase = new UseCase(csvResource, loader, dao, jsonMapper);
        useCase.run();
    }

    InputStream getCsvResource() {
        return getClass().getResourceAsStream("/persons.csv");
    }

    PersonRepo getPersonRepo() {
        var repo = new JdbcClassicPersonRepo();
        repo.setMapper(getJdbcMapper());
        repo.setDataSource(getDataSource());
        repo.createTable();
        return repo;
    }

    PersonLoader getPersonLoader() {
        var loader = new PersonLoader();
        loader.setMapper(getCsvMapper());
        return loader;
    }

    Connection getDataSource() {
        return new H2DataSourceBuilder().openServer();
    }

    PersonCsvMapper getCsvMapper() {
        return new PersonCsvMapper(";");
    }

    PersonJdbcMapper getJdbcMapper() {
        return new PersonJdbcMapper();
    }

    PersonJsonMapper getJsonMapper() {
        return new PersonJsonMapper();
    }
*/


}

