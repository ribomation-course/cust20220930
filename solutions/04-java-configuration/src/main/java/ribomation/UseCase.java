package ribomation;

import ribomation.domain.Person;
import ribomation.domain.PersonJsonMapper;
import ribomation.domain.PersonLoader;
import ribomation.domain.PersonRepo;

import java.io.InputStream;

public class UseCase {
    InputStream csvResource;
    PersonLoader loader;
    PersonRepo dao;
    PersonJsonMapper jsonMapper;

    public UseCase(InputStream csvResource, PersonLoader loader, PersonRepo dao, PersonJsonMapper jsonMapper) {
        this.csvResource = csvResource;
        this.loader      = loader;
        this.dao         = dao;
        this.jsonMapper  = jsonMapper;
    }

    public void run() {
        loader.load(csvResource, dao::insert);
        System.out.printf("inserted %d persons%n", dao.countAll());

        var lst = dao.findByAgeBetweenAndPostCodeLessThanAndFemale(30, 40, 12_500);
        System.out.printf("found %d persons%n", lst.size());

        lst.forEach(p -> dao.delete(p.getId()));
        System.out.printf("removed %d persons%n", lst.size());
        System.out.printf("reduced size to %d persons%n", dao.countAll());

        System.out.printf("found %s%n", dao.findById(1).orElseThrow());
        dao.update(1, new Person("Per Silja", 42, false, 25_000));
        System.out.printf("modified %s%n", dao.findById(1).orElseThrow());

        System.out.println(jsonMapper.toJson(lst));
    }
}


