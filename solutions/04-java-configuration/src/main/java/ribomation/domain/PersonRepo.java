package ribomation.domain;

import java.util.List;
import java.util.Optional;

public interface PersonRepo {
    public int countAll();

    public List<Person> findAll();

    public Optional<Person> findById(int id);

    public List<Person> findByAgeBetweenAndPostCodeLessThanAndFemale(int ageLower, int ageUpper, int postCodeUpper);

    int insert(Person person);

    void update(int id, Person person);

    void delete(int id);

}
