package ribomation.boot_web_spa;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import ribomation.boot_web_spa.domain.PersonDAO;

@SpringBootApplication
public class Application implements WebMvcConfigurer {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    CommandLineRunner setup(PersonDAO dao) {
        return args -> {
            dao.populate(5);
            System.out.printf("generated %d persons%n", dao.count());
        };
    }
}
