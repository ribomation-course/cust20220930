package ribomation.boot_web_spa.domain;

import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class PersonGenerator {
    private static final String[] names = {
            "anna", "berit", "carin", "doris", "eva", "frida", "gudrun",
            "anders", "bertil", "carl", "david", "erik", "fred", "gunnar"
    };
    private static final String[] companies = {
            "ABB", "Bofors", "Cementa", "DeLaval", "Ericsson", "Fazer",
            "SKF", "Atlas-Copco", "Telia", "IKEA", "SEB", "Handelsbanken"
    };
    private static final String[] domains = {
            "se", "com", "co.uk", "nu"
    };
    private static final Random r = new Random();
    private static int nextId = 1;

    private String pick(String[] arr) {
        return arr[r.nextInt(arr.length)];
    }

    private String toCap(String s) {
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }

    public Integer id() {
        return nextId++;
    }

    public String firstName() {
        return toCap(pick(names));
    }

    public String lastName() {
        return toCap(pick(names)) + "son";
    }

    public Integer age() {
        return 20 + r.nextInt(65);
    }

    public String company() {
        return toCap(pick(companies));
    }

    public String email(String fn, String ln, String co, String dom) {
        return String.format("%s.%s@%s.%s", fn, ln, co, dom).toLowerCase();
    }

    public String email(String fn, String ln, String co) {
        return email(fn, ln, co, pick(domains));
    }

    public String email(String fn, String ln) {
        return email(fn, ln, company(), pick(domains));
    }

    public String avatar(String email, int size) {
        var url = "https://robohash.org";
        var setNo = 1;
//        return String.format("%s/%s.png?set=set%d&size=%dx%d", url, email, setNo, size, size);
        return String.format("%s/%s.png?set=set%d", url, email, setNo);
    }

    public String avatar(String email) {
        return avatar(email, 200);
    }

    public Person person() {
        var id = id();
        var fn = firstName();
        var ln = lastName();
        var na = String.format("%s %s", fn, ln);
        var ag = age();
        var co = company();
        var em = email(fn, ln, co);
        var av = avatar(em);
        return new Person(id, na, ag, av, co, em);
    }

    public Person personEmpty() {
        var id = id();
        var fn = firstName();
        var ln = lastName();
        var co = company();
        var em = email(fn, ln, co);
        var av = avatar(em);
        return new Person(id, "", 0, av, "", "");
    }


}
