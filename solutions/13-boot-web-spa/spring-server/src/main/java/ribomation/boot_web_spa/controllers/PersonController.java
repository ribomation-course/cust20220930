package ribomation.boot_web_spa.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ribomation.boot_web_spa.domain.Person;
import ribomation.boot_web_spa.domain.PersonDAO;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/persons")
@CrossOrigin
public class PersonController {
    @Autowired
    private PersonDAO dao;

    static class NotFound extends RuntimeException {
        final int id;

        NotFound(int id) {
            super("item with id=" + id + " not found");
            this.id = id;
        }
    }

    @GetMapping
    public List<Person> list() {
        return dao.findAll();
    }

    @GetMapping("/{id}")
    public Person show(@PathVariable int id) {
        if (id == -1) {
            return dao.create();
        } else {
            return dao.findById(id).orElseThrow(() -> new NotFound(id));
        }
    }

    @PutMapping("/{id}")
    public Person update(@PathVariable int id, @RequestBody Person delta) {
        dao.update(id, delta);
        return dao.findById(id).orElseThrow(() -> new NotFound(id));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable int id) {
        dao.findById(id).orElseThrow(() -> new NotFound(id));
        dao.delete(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Person save(@RequestBody Person delta) {
        var p = dao.create();
        dao.insert(p);
        dao.update(p.getId(), delta);
        return dao.findById(p.getId()).orElseThrow(() -> new NotFound(p.getId()));
    }

    @ExceptionHandler(NotFound.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Map<String, Integer> notFound(NotFound x) {
        return Map.of("id", x.id);
    }

}
