package ribomation.boot_web_spa.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class PersonDAO {
    private final List<Person> personList = new ArrayList<>();

    @Autowired
    private PersonGenerator generator;

    public void populate(int count) {
        while (count-- > 0) {
            personList.add(generator.person());
        }
    }

    public void populate() {
        populate(10);
    }

    public int count() {
        return personList.size();
    }

    public List<Person> findAll() {
        return personList;
    }

    public Optional<Person> findById(int id) {
        return personList.stream().filter(p -> p.getId().equals(id)).findFirst();
    }

    public boolean existsById(int id) {
        return personList.stream().anyMatch(p -> p.getId().equals(id));
    }

    public int insert(Person p) {
        p.setId(generator.id());
        personList.add(p);
        return p.getId();
    }

    public Person create() {
        var p = generator.personEmpty();
        p.setId(-1);
        return p;
    }

    public void delete(int id) {
        personList.removeIf(p -> p.getId().equals(id));
    }

    public void update(int id, Person delta) {
        var obj = findById(id);
        if (obj.isEmpty()) {
            throw new RuntimeException("not found: " + id);
        }

        var person = obj.get();
        if (delta.getName() != null) {
            person.setName(delta.getName());
        }
        if (delta.getAge() != null && delta.getAge() > 0) {
            person.setAge(delta.getAge());
        }
        if (delta.getCompany() != null) {
            person.setCompany(delta.getCompany());
        }
        if (delta.getEmail() != null) {
            person.setEmail(delta.getEmail());
        }
    }

}
