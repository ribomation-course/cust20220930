import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { NotFoundPage } from './pages/not-found/not-found.page';
import { ListPage } from './pages/list/list.page';
import { ShowPage } from './pages/show/show.page';
import { EditPage } from './pages/edit/edit.page';

@NgModule({
    declarations: [
        AppComponent,
        NotFoundPage,
        ListPage,
        ShowPage,
        EditPage
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
