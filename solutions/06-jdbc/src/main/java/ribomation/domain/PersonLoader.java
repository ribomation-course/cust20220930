package ribomation.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class PersonLoader {
    public interface Handler {
        void onPersonReceived(Person person);
    }

    private PersonCsvMapper mapper;

    @Autowired
    public void setMapper(PersonCsvMapper mapper) {
        this.mapper = mapper;
    }

    public Collection<Person> loadAll(InputStream is)  {
        try (var in = new BufferedReader(new InputStreamReader(is))) {
            return in.lines()
                    .skip(1)
                    .map(mapper::fromCSV)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void load(InputStream is, Handler handler) {
        load(new BufferedReader(new InputStreamReader(is)).lines(), handler);
    }

    private void load(Stream<String> lines, Handler handler) {
       final var cnt = new int[]{0};
        lines
                .skip(1)
                .map(mapper::fromCSV)
                .peek(p -> System.out.printf("insert (%d): %s%n", ++cnt[0], p))
                .forEach(handler::onPersonReceived);
    }

}
