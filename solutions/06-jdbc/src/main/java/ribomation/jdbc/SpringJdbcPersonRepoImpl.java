package ribomation.jdbc;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import ribomation.domain.Person;
import ribomation.domain.PersonRepo;

import javax.sql.DataSource;
import java.sql.Statement;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public class SpringJdbcPersonRepoImpl implements PersonRepo, InitializingBean {
    private final JdbcTemplate jdbc;
    private final RowMapper<Person> mapper;
    private final String tableName = "persons";
    private final String tableSQL = """
            DROP TABLE IF EXISTS persons;
            CREATE TABLE persons (
                id          int primary key auto_increment,
                name        varchar(64),
                age         int,
                gender      varchar(6),
                postCode    int
            );
            """;

    public SpringJdbcPersonRepoImpl(DataSource ds, RowMapper<Person> mapper) {
        jdbc = new JdbcTemplate(ds);
        this.mapper = mapper;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        createTable();
    }

    public void createTable() {
        jdbc.execute(tableSQL);
    }

    @Override
    public int countAll() {
        var sql = "SELECT count(*) FROM " + tableName;
        return jdbc.queryForObject(sql, Integer.class);
    }

    @Override
    public List<Person> findAll() {
        var sql = "SELECT * FROM " + tableName;
        return jdbc.query(sql, mapper);
    }

    @Override
    public Optional<Person> findById(int id) {
        var sql = "SELECT * FROM " + tableName + " where id = ?";
        try {
            var p = jdbc.queryForObject(sql, mapper, id);
            assert p != null;
            return Optional.of(p);
        } catch (DataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public List<Person> findByAgeBetweenAndPostCodeLessThanAndFemale(int ageLower, int ageUpper, int postCodeUpper) {
        var sql = "SELECT id,name,age,gender,postCode FROM " + tableName + " WHERE age BETWEEN ? AND ? AND postCode <= ? AND gender = ?";
        return jdbc.query(sql, mapper, ageLower, ageUpper, postCodeUpper, "Female");
    }

    @Override
    public int insert(Person p) {
        PreparedStatementCreator psc = con -> {
            var sql = "insert into " + tableName + " (name,age,gender,postcode) values (?,?,?,?)";
            var ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, p.getName());
            ps.setInt(2, p.getAge());
            ps.setString(3, p.isFemale() ? "Female" : "Male");
            ps.setInt(4, p.getPostCode());
            return ps;
        };
        var keyHolder = new GeneratedKeyHolder();
        jdbc.update(psc, keyHolder);
        return keyHolder.getKey().intValue();
    }

    /**
     * Insert a bunch of domain objects, using the batchUpdate() method of JdbcTemplate
     * @see <a href="https://www.baeldung.com/spring-jdbc-batch-inserts">...</a>
     */
    @Override
    public void insert(Collection<Person> personCollection) {
        final var sql = "insert into " + tableName + " (name,age,gender,postcode) values (?,?,?,?)";
        final var batchSize = 1000;
        jdbc.batchUpdate(sql, personCollection, batchSize, (ps, p) -> {
            ps.setString(1, p.getName());
            ps.setInt(2, p.getAge());
            ps.setString(3, p.isFemale() ? "Female" : "Male");
            ps.setInt(4, p.getPostCode());
        });
    }

    @Override
    public void update(int id, Person p) {
        var sql = "UPDATE " + tableName + " SET name=?, age=?, gender=?, postCode=? WHERE id=?";
        jdbc.update(sql, p.getName(), p.getAge(), p.isFemale() ? "Female" : "Male", p.getPostCode(), p.getId());
    }

    @Override
    public void delete(int id) {
        var sql = "DELETE FROM " + tableName + " WHERE id=?";
        jdbc.update(sql, id);
    }

}
