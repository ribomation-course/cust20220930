package ribomation.jdbc;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import ribomation.domain.Person;
import ribomation.domain.PersonRepo;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Deprecated
public class JdbcClassicPersonRepo
        implements PersonRepo, InitializingBean
{
    final String tableName = "persons";
    final String tableSQL = """
            DROP TABLE IF EXISTS persons;
            CREATE TABLE persons (
                id          int primary key auto_increment,
                name        varchar(64),
                age         int,
                gender      varchar(6),
                postCode    int
            );
            """;

    private Connection dataSource;
    private PersonJdbcMapper mapper;

    public JdbcClassicPersonRepo() {
    }

//    @Autowired
//    @Qualifier("h2")
    public void setDataSource(H2DataSourceBuilder dataSource) {
        this.dataSource = dataSource.openServer();
    }

//    @Autowired
//    @Qualifier("jdbcMapper")
    public void setMapper(PersonJdbcMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        createTable();
    }

    public void close() {
        if (dataSource != null) {
            try {
                dataSource.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void createTable() {
        try {
            var stmt = dataSource.createStatement();
            try (stmt) {
                stmt.executeUpdate(tableSQL);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public int insert(Person person) {
        try {
            var sql = "INSERT INTO " + tableName + " (name,age,gender,postCode) VALUES (?,?,?,?)";
            var stmt = dataSource.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            try (stmt) {
                mapper.prepareInsert(stmt, person).executeUpdate();
                var rs = stmt.getGeneratedKeys();
                try (rs) {
                    if (rs.next()) {
                        return rs.getInt(1);
                    } else {
                        throw new RuntimeException("no PK returned");
                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void insert(Collection<Person> person) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(int id, Person person) {
        try {
            var sql = "UPDATE " + tableName + " SET name=?, age=?, gender=?, postCode=? WHERE id=?";
            var stmt = dataSource.prepareStatement(sql);
            try (stmt) {
                var cnt = mapper.prepareUpdate(stmt, id, person).executeUpdate();
                if (cnt != 1) {
                    throw new RuntimeException("update failed: count=" + cnt);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(int id) {
        try {
            var sql = "DELETE FROM " + tableName + " WHERE id=?";
            var stmt = dataSource.prepareStatement(sql);
            try (stmt) {
                stmt.setInt(1, id);
                var cnt = stmt.executeUpdate();
                if (cnt != 1) {
                    throw new RuntimeException("delete failed: count=" + cnt);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public int countAll() {
        try {
            var sql = "SELECT count(*) FROM " + tableName;
            var stmt = dataSource.createStatement();
            try (stmt) {
                var rs = stmt.executeQuery(sql);
                try (rs) {
                    if (rs.next()) {
                        return (int) rs.getLong(1);
                    } else {
                        throw new RuntimeException("no COUNT(*) returned");
                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Person> findAll() {
        try {
            var sql = "SELECT id,name,age,gender,postCode FROM " + tableName;
            var stmt = dataSource.createStatement();
            try (stmt) {
                var rs = stmt.executeQuery(sql);
                try (rs) {
                    var result = new ArrayList<Person>();
                    while (rs.next()) {
                        result.add(mapper.fromResultSet(rs));
                    }
                    return result;
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<Person> findById(int id) {
        try {
            var sql = "SELECT id,name,age,gender,postCode FROM " + tableName + " WHERE id=" + id;
            var stmt = dataSource.createStatement();
            try (stmt) {
                var rs = stmt.executeQuery(sql);
                try (rs) {
                    if (rs.next()) {
                        return Optional.of(mapper.fromResultSet(rs));
                    } else {
                        return Optional.empty();
                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Person> findByAgeBetweenAndPostCodeLessThanAndFemale(int ageLower, int ageUpper, int postCodeUpper) {
        try {
            var sql = "SELECT id,name,age,gender,postCode FROM " + tableName + " WHERE age BETWEEN ? AND ? AND postCode <= ? AND gender = 'Female'";
            var stmt = dataSource.prepareStatement(sql);
            try (stmt) {
                stmt.setInt(1, ageLower);
                stmt.setInt(2, ageUpper);
                stmt.setInt(3, postCodeUpper);

                var rs = stmt.executeQuery();
                try (rs) {
                    var result = new ArrayList<Person>();
                    while (rs.next()) {
                        result.add(mapper.fromResultSet(rs));
                    }
                    return result;
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
