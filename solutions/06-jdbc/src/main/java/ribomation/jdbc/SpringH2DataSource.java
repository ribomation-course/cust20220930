package ribomation.jdbc;

import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

@Component
public class SpringH2DataSource extends DriverManagerDataSource {
    static final String url = "jdbc:h2:tcp://localhost:9092/persons";
    static final String usr = "sa";
    static final String pwd = "";

    public SpringH2DataSource() {
        super(url, usr, pwd);
    }
}
