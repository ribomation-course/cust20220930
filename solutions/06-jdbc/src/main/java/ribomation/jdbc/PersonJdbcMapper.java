package ribomation.jdbc;

import org.springframework.stereotype.Service;
import ribomation.domain.Person;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Deprecated
//@Service("jdbcMapper")
public class PersonJdbcMapper {

    public Person fromResultSet(ResultSet rs) {
        try {
            var id = rs.getInt(1);
            var name = rs.getString(2);
            var age = rs.getInt(3);
            var gender = rs.getString(4);
            var postCode = rs.getInt(5);
            return Person.of(id, name, age, gender, postCode);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public PreparedStatement prepareInsert(PreparedStatement ps, Person person) {
        try {
            ps.setString(1, person.getName());
            ps.setInt(2, person.getAge());
            ps.setString(3, person.isFemale() ? "Female" : "Male");
            ps.setInt(4, person.getPostCode());
            return ps;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public PreparedStatement prepareUpdate(PreparedStatement ps, int id, Person person) {
        try {
            ps.setString(1, person.getName());
            ps.setInt(2, person.getAge());
            ps.setString(3, person.isFemale() ? "Female" : "Male");
            ps.setInt(4, person.getPostCode());
            ps.setInt(5, id);
            return ps;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


}
