package ribomation.jdbc;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ribomation.domain.Person;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class SpringPersonRowMapper implements RowMapper<Person> {
    @Override
    public Person mapRow(ResultSet rs, int rowNum) throws SQLException {
        var name = rs.getString("name");
        var age = rs.getInt("age");
        var female = rs.getString("gender").equalsIgnoreCase("female");
        var postCode = rs.getInt("postCode");
        return new Person(name, age, female, postCode);
    }
}
