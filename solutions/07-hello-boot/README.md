# Run  using Gradle

    ./gradlew bootRun

# Build & Run Executable JAR

    ./gradlew build
    java -jar build/libs/*SNAPSHOT.jar

