package ribomation.hello_boot;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    CommandLineRunner doit() {
        return args -> {
            System.out.println("---------------------------------------");
            System.out.printf("Hi there, from a Spring Boot App !!%n");
            System.out.println("---------------------------------------");
        };
    }
}
