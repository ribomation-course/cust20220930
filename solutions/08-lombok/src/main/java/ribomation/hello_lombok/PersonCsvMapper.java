package ribomation.hello_lombok;

import org.springframework.stereotype.Service;

@Service
public class PersonCsvMapper {
    public final String defaultDelim = ";";
    private static int nextId = 1;

    public Person fromCSV(String csv) {
        return fromCSV(csv, defaultDelim);
    }

    public Person fromCSV(String csv, String delim) {
        var f = csv.split(delim);
        var ix = 0;

        //name;age;gender;postCode
        var name = f[ix++];
        var age = f[ix++];
        var gender = f[ix++];
        var postCode = f[ix++];

        try {
            return Person.of(nextId++, name, age, gender, postCode);
        } catch (NumberFormatException e) {
            throw new RuntimeException("invalid numeric field", e);
        }
    }
}
