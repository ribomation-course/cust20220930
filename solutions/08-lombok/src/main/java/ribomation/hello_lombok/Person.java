package ribomation.hello_lombok;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Person {
    //name;age;gender;postCode
    private int id;
    private String name;
    private int age;
    private boolean female;
    private int postCode;

    public static Person of(int id, String name, String age, String gender, String postCode) {
        return of(id, name, Integer.parseInt(age), gender.equals("Female"), Integer.parseInt(postCode));
    }

    public static Person of(int id, String name, int age, boolean female, int postCode) {
        return new Person(id, name, age, female, postCode);
    }
}
