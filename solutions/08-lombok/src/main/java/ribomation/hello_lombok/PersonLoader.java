package ribomation.hello_lombok;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class PersonLoader {
    private final PersonCsvMapper mapper;

    public PersonLoader(PersonCsvMapper mapper) {
        this.mapper = mapper;
    }

    public Collection<Person> loadAll(InputStream is)  {
        try (var in = new BufferedReader(new InputStreamReader(is))) {
            return in.lines()
                    .skip(1)
                    .map(mapper::fromCSV)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
