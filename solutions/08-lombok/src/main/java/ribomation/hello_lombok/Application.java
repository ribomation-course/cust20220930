package ribomation.hello_lombok;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.util.Collection;

@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    CommandLineRunner load_and_print(PersonLoader loader) {
        return args -> {
            var N = args.length == 0 ? 50 : Integer.parseInt(args[0]);
            var r = new ClassPathResource("/persons.csv");
            loader.loadAll(r.getInputStream()).stream()
                    .limit(N)
                    .forEach(System.out::println);
        };
    }
}
