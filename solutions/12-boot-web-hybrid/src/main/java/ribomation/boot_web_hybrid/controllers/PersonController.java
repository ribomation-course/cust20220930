package ribomation.boot_web_hybrid.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ribomation.boot_web_hybrid.domain.PersonDAO;

@Controller
@RequestMapping("/")
public class PersonController {
    @Autowired
    private PersonDAO dao;

    @GetMapping("show/{id}")
    public String show(@PathVariable int id, Model m) {
        if (dao.existsById(id)) {
            m.addAttribute("id", id);
            return "show";
        }
        return "redirect:/list";
    }

    @GetMapping("edit/{id}")
    public String edit(@PathVariable int id, Model m) {
        if (dao.existsById(id)) {
            m.addAttribute("id", id);
            return "edit";
        }
        return "redirect:/list";
    }

    @GetMapping("create")
    public String create(Model data) {
        data.addAttribute("id", -1);
        return "edit";
    }

}
