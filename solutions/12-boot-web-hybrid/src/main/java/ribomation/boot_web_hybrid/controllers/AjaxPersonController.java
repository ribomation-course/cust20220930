package ribomation.boot_web_hybrid.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ribomation.boot_web_hybrid.domain.Person;
import ribomation.boot_web_hybrid.domain.PersonDAO;

import java.util.List;

@RestController
@RequestMapping("/api/persons")
public class AjaxPersonController {
    @Autowired
    private PersonDAO dao;

    @GetMapping
    public List<Person> list() {
        return dao.findAll();
    }

    @GetMapping("/{id}")
    public Person show(@PathVariable int id) {
        if (id == -1) {
            return dao.create();
        } else {
            return dao.findById(id).orElseThrow();
        }
    }

    @PutMapping("/{id}")
    public Person update(@PathVariable int id, @RequestBody Person delta) {
        dao.update(id, delta);
        return dao.findById(id).orElseThrow();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable int id) {
        dao.delete(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Person save(@RequestBody Person delta) {
        var p = dao.create();
        dao.insert(p);
        dao.update(p.getId(), delta);
        return dao.findById(p.getId()).orElseThrow();
    }

}
