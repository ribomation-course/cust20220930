package ribomation.boot_web_hybrid;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import ribomation.boot_web_hybrid.domain.PersonDAO;

@SpringBootApplication
public class Application implements WebMvcConfigurer {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("redirect:/list");
        registry.addViewController("/list").setViewName("list");
    }

    @Bean
    CommandLineRunner setup(PersonDAO dao) {
        return args -> {
            dao.populate(5);
            System.out.printf("generated %d persons%n", dao.count());
        };
    }
}
