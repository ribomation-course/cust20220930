package ribomation.jdbc;

import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Service("h2")
public class H2DataSourceBuilder {
    private final String usr = "sa";
    private final String pwd = "";

    public Connection openServer() {
        return openServer("persons");
    }

    public Connection openServer(String db) {
        return openServer("localhost", 9092, db);
    }

    public Connection openServer(String host, int port, String db) {
        try {
            var url = String.format("jdbc:h2:tcp://%s:%d/%s", host, port, db);
            return DriverManager.getConnection(url, usr, pwd);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Connection openMemory() {
        return openMemory("persons");
    }

    public Connection openMemory(String db) {
        try {
            var url = "jdbc:h2:mem:" + db;
            return DriverManager.getConnection(url, usr, pwd);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Connection openFolder(String dir, String db) {
        try {
            var url = String.format("jdbc:h2:%s/%s", dir, db);
            return DriverManager.getConnection(url, usr, pwd);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
